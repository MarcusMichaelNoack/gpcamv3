################################################################
######Run SMART Visualization###################################
################################################################
import Config as conf

import sys
sys.path.insert(0, './src/Visualization')


if conf.TimeSeries[0] == True: 
    import PlotCurve
else:
    import Evaluate


