#!/usr/bin/python3

import numpy as np

# Define the physical system
########################################
gas_constant = 8.31446261815324e-3 # kJ/mol*K
physical_parameters = {
    'alpha' : 0.2 , # Grain growth scaling exponent [dimensionless]
    'adhoc_late_stage' : [0.1, 1000, 10000] , # Late stage growth
    'A' : 30.e10 , # Arrhenius prefactor [nm/s^alpha]
    'Ea' : 100.0 , # Activation energy [kJ/mol]
    'T_ODT' : 350 , # Order-disorder transition temperature [C]
    'ODT_prox' : 20 , # Near-ODT region where kinetics are suppressed [C]
    'dissolution' : 1.0 , # Grain dissolution near ODT [nm/s*K]
    'minimum_grain_size' : 60 , # nm
    }




# These functions define the physical evolution of the system for a given set of T(t) conditions
########################################
def evolve_structure(current_state, physical_parameters, dt, state_key=None, subdivision=1000):
    
    sequence = []
    
    dt = dt/subdivision
    for i in range(int(subdivision)):
        current_state = evolve_structure_micro(current_state, physical_parameters, dt)
        
        if state_key is not None:
            sequence.append(current_state[state_key])
    
    return current_state, sequence
    


def evolve_structure_micro(current_state, physical_parameters, dt):
    
    grain_size = current_state['grain_size']
    T_K = current_state['T'] + 273.15 # K
    T_above_ODT = current_state['T'] - physical_parameters['T_ODT']
    
    
    alpha = physical_parameters['alpha']
    # Account for 'late stage slowing down' of kinetics
    alpha2, lim1, lim2 = physical_parameters['adhoc_late_stage']
    if grain_size>lim2:
        alpha = alpha2
    elif grain_size>lim1:
        extent = (grain_size-lim1)/(lim2-lim1)
        alpha = alpha*(1-extent) + alpha2*extent
    
    # Account for proximity to ODT
    prox = physical_parameters['ODT_prox']
    if T_above_ODT>-prox:
        # Near ODT, kinetics are disrupted
        extent = ( (T_above_ODT+prox)/prox )
        T_K = np.clip( T_K*( 1-np.power(extent,2) ), 1e-10, T_K )
    
    Ea_over_R = physical_parameters['Ea']/gas_constant
    
    k = physical_parameters['A']*np.exp(-Ea_over_R/T_K)

    
    # Increase in grain_size due to coarsening kinetics
    growth_rate = k**(1/alpha) * grain_size**(1 - 1/alpha) # nm/s
    
    if T_above_ODT>0:
        # We are above ODT; grains 'dissolve'
        growth_rate = -physical_parameters['dissolution']*T_above_ODT # nm/s
        
        
    growth_rate = np.clip(growth_rate, -1e6, 1e4)
    current_state['grain_size'] = np.clip(grain_size + growth_rate*dt, physical_parameters['minimum_grain_size'], 1e6)
    
    return current_state





# Define the time/Temperature history; a.k.a. T(t) curve
########################################
def res(T_curve):
    num, dt = 10, 6
    #num, dt = 20, 15
    time_intervals = np.ones((num))*dt
    #time_intervals[5] = dt*5 # Test having the dt be different


    # Constant temperature:
    #T_curve = np.ones((num))*250
    #T_curve = np.ones((num))*330.4

    # Linear ramp:
    #T_curve = np.linspace(0, 500, num=num)

    # Complex T sequence:







    # Run the simulation for this T(t) curve
    ########################################
    initial_state = {'grain_size' : 80, 'T' : 25.0 }
    state_key = 'grain_size'


    time_curve = [ np.sum(time_intervals[:i]) for i, t in enumerate(time_intervals) ]
    current_state = initial_state.copy()
    states = [ current_state[state_key] ]
    states_dense = []
    for i, (dt, t, T) in enumerate(zip(time_intervals, time_curve, T_curve)):
        
        original_state = current_state[state_key]
        
        # Compute changes to the structure
        current_state['T'] = T
        current_state, sequence = evolve_structure(current_state, physical_parameters, dt, state_key=state_key)
        
        states.append(current_state[state_key])
        states_dense += sequence
        
        final_state = current_state[state_key]
        
        
    states = np.asarray(states)
    states_dense = np.asarray(states_dense)
    #print(states_dense)
    #input()
    return time_intervals,T_curve,states,states_dense

#T_curve = [25, 200, 270, 200, 350.5, 360, 350, 200, 544,322]
#Stime_intervals, T_curve ,states,states_dense = res(T_curve)

# Plot the results
########################################
if True:
    # Plot results
    import matplotlib as mpl
    mpl.rcParams['mathtext.fontset'] = 'cm'
    import pylab as plt
    
    mpl.rcParams['xtick.labelsize'] = 15
    mpl.rcParams['ytick.labelsize'] = 15

    def plot(time_intervals, T_curve, states, states_dense=None, outfile='history.png', size=10.0, plot_buffers=[0.12,0.12,0.1,0.05], dpi=200):
        
        mpl.rcParams['xtick.labelsize'] = 15
        mpl.rcParams['ytick.labelsize'] = 15
        fig = plt.figure(figsize=(size,size*3/4), facecolor='white')
        left_buf, right_buf, bottom_buf, top_buf = plot_buffers
        fig_width = 1.0-right_buf-left_buf
        fig_height = 1.0-top_buf-bottom_buf
        ax = fig.add_axes( [left_buf, bottom_buf, fig_width, fig_height] )
        
        
        x = []
        y = []
        time_total = 0
        for t, T in zip(time_intervals, T_curve):
            x.append(time_total)
            y.append(T)
            
            time_total += t
            
            x.append(time_total)
            y.append(T)

        ax.plot(x, y, 'o-', color='r', linewidth=2.0)
        ax.set_xlabel('$t \, (\mathrm{s})$', size=20)
        ax.set_ylabel('$T \, (^{\circ}C)$', color='r', size=20)
        xi, xf, yi, yf = ax.axis()
        #xi, xf, yi, yf = 0, np.max(x), 0, yf
        #ax.axis([xi,xf,yi,yf])
        
        
        ax2 = ax.twinx()

        time_curve = [ np.sum(time_intervals[:i]) for i, t in enumerate(time_intervals) ]
        time_curve.append( np.sum(time_intervals) )
        
        ax2.plot(time_curve, states, 'o-', color='b', linewidth=1.0)
        ax2.set_ylabel('state', color='b', size=20)
        
        if states_dense is not None:
            subdivision = int( len(states_dense)/(len(states)-1) )
            for i, (dt, t) in enumerate(zip(time_intervals, time_curve)):
                y = states_dense[i*subdivision:(i+1)*subdivision]
                x = np.linspace(0,dt, num=len(y)) + t
                ax2.plot(x, y, '-', color='purple', linewidth=1.0)
        
        plt.savefig(outfile, dpi=dpi)
        plt.show()
        plt.close()

        
        

    #plot(time_intervals, T_curve, states, states_dense=states_dense)
    
    
    
