import numpy as np


def expert_function1(p1,p2,coefficients, *argv):
    ###expert knowledge is given by a mean and a co-variance vector
    ###To create the co-variance, the kernel trip is used
    ###make sure the kernel is symmetric in p1 and p2
    delta_fv = (coefficients[0]*abs(p1[0] - p2[0])) + (coefficients[1]*abs(p1[1] - p2[1]))
    covariance = np.exp(-np.linalg.norm(np.subtract(p1,p2))) # * delta_fv
    mean = (coefficients[0]*p1[0]) + coefficients[1]
    variance = 0.5 ##This is a placeholder!! Here we have to propagate the variances in the coefficients to the variance at point p
    if all(p1 == p2) == True: covariance = covariance + variance
    return mean, covariance

