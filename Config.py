###########################################
###Configuration File######################
###########################################
import numpy as np
###############################
###General#####################
###############################
Parameters = {
                'x_position':
                {
                    'elements':[-0.8,0.8],       ####either an interval, several intervals or discrete points
                    'prepared_elements':[-0.8,0.8], ####either an interval, several intervals or discrete points, subset of 'elements'
                    'property':'connected',        ####connected, discrete, or disconnected
                    'resolution':0.025,         # 26-27 um horizontal beam size
                    'costs':['linear',0.1]      # motor speed = 10 mm/s
                },
                'y_position':
                {
                    'elements':[-0.8,0.8],
                    'prepared_elements':[-0.8,0.8],
                    'property':'connected',
                    'resolution':0.002,         #1.4 um vertical beam size
                    'costs':['linear',1.0]      # motor speed = 1 mm/s
                },
             }

ModelFunctions = {
                    'eta':
                    {
                        'use for steering': True,
                        'expert knowledge': {
                                'switch': 'off',
                                'expert models':{
                                    'expert_model_1':{
                                        'source':'simulation',  #'points' or 'simulation'
                                        'path':'./expert_knowledge/CS_expert.npy',#the data file must have right dict key
                                        },
                                    },
                                'bounds':[-10.0,10.0],
                                'differentiability': {'x_position': 0, 'y_position': 0}   ###in the order or the parameters, 0,1,2 or 'inf'
                                }
                    },
                    #'orientation_factor':
                    #{
                    #    'use for steering': True,
                    #    'expert knowledge': {
                    #            'switch': 'off',
                    #            'expert models':{
                    #                'expert_model_1':{
                    #                    'source':'simulation',  #'points' or 'simulation'
                    #                    'path':'./expert_knowledge/CS_expert.npy',#the data file must have right dict key
                    #                    },
                    #                },
                    #            'bounds':[-10.0,10.0],
                    #            'differentiability': {'x_position': 0, 'y_position': 0}   ###in the order or the parameters, 0,1,2 or 'inf'
                    #            }
                    #},
                 }

breaking_error = 1e-8
WaitingTimeOut = [False,3]
Engine = "GP"  ###"GP" or "OK"
TimeSeries = [False,[0,6,12]]  ###time series experiments and when [sec] to perform them
########################################
###Variance OPTIMIZATION################
########################################
population_size = 20 ##recommendation 10*number of parameters, but max 100000
GradientDistributionCoefficients= [0.6,1.0,0.02,0.5]
TargetDistributionCoefficients  = [0.6,1.0,0.02,1.0]
SimpleDistributionSelection = [True,0.8,0.8]    ##overwrites the two preceding parameters if True

model_for_gradient = 'orientation_factor' # change this for gradient
model_for_target = 'eta'
target_sign = +1   ##looking for maximum (+1) or minimum (-1)
GradientMode = False
TargetMode = False
ModelOptimizationMethod = 'Genetic'      ##'Genetic' or 'HGDN'
ErrorOptimizationMethod = 'Genetic'       ##'Genetic' or 'HGDN'
OptimaList_Length = 10      # not used for Generic
########################################
###Hyper-Parameter OPTIMIZATION#########
########################################
OptimizationBounds = [[0.00001,300],[0.001,500.0],[0.001,500.0]]    ###first entry for signal variance, then upper bound of length scales in the order of parameters, if unknown set to size of domain

###############################
###EXPERIMENT##################
###############################

Overlap_mode = True #True #In this mode SMART prediction is overlapped with the beam line experiment

GetDataFrom = 'synthetic'  #'synthetic', 'experiment' or 'interpolated'
DataFile = './data/example_data/CoffeeStain.npy'  #used if 'interpolated' was selected; NOT used for 'experiment'
wait_for_experiment = 1  #how frequently the algorithm checks if the measurement result is available, in [sec]
Max_Waiting_Time = 1800  ##waiting time for new experiment data before data is re-chosen
number_of_predicted_experiments = 10 ##only if optimization is HGDN
#Costs = False
Costs = False
#AdjustCosts = [False,'time']
AdjustCosts = [False,'time']
CostOffset = 1.5          # fixed cost per point in [sec]
###############################
###DATA ACQUISITION############
###############################
initial_data_set_size = 10
Limited_Number_of_Measurements = True
Max_Number_of_Measurements = 90
ReadData = False
ask_for_file = False    # not used
data_file = "InputExp_AFRL.npy" 

###############################
###Computation#################
###############################
RecomputeKernel = 0.9        #as a fraction how much the amount of data has changed before new hyper-parameters are computed
Rank_n_Update = True
Compact_Kernel = False       #Part of future version
GPU_Acceleration =   False   #Part of future version
HyperParameterUpdateRecomputationRatio = 0.9
SystemSolver = 'INV'         #"INV", "CG" or "MINRES"   #not implemented yet


###############################
###Domain Awareness############
###############################
#not functional at the moment
GetSimulationDataFrom = 'synthetic'     # not used yet



##########################
#####Visualization########
##########################
Plotting_Parameters = {
                'x_position':
                {
                    'bounds': [-0.8,0.8],
                    'size': 50,
                    'plot': [True],
                },
                'y_position':
                {
                    'bounds': [-0.8,0.8],
                    'size': 50,
                    'plot': [True],
                }
            }
Plotting_ModelFunctions = {
                'eta':
                {
                    'plot model function': True,
                    'plot variance function': False,
                    'plot model gradient': False,
                    'plot variance gradient': False,
                    'plot cost function': False,
                    'plot objective function': False,
                    'plot objective gradient': False,
                    'plot black-box function': False,
                    'plot as image': False,
                    'plot density': False,
                },
                'orientation_factor':
                {
                    'plot model function': True,
                    'plot variance function': False,
                    'plot model gradient': False,
                    'plot variance gradient': False,
                    'plot cost function': False,
                    'plot objective function': False,
                    'plot objective gradient': False,
                    'plot black-box function': False,
                    'plot as image': False,
                    'plot density': False,
                },
#                'sigma':
#                {
#                    'plot model function': False,
#                    'plot variance function': False,
#                    'plot model gradient': False,
#                    'plot variance gradient': False,
#                    'plot cost function': False,
#                    'plot objective function': False,
#                    'plot objective gradient': False,
#                    'plot black-box function': False,
#                    'plot as image': False,
#                    'plot density': False,
#                },
                #'orientation_angle':
                #{
                #    'plot model function': True,
                #    'plot variance function': False,
                #    'plot model gradient': False,
                #    'plot variance gradient': False,
                #    'plot cost function': False,
                #    'plot objective function': False,
                #    'plot objective gradient': False,
                #    'plot black-box function': False,
                #    'plot as image': False,
                #    'plot density': False,
                #},
                #'prefactor':
                #{
                #    'plot model function': True,
                #    'plot variance function': False,
                #    'plot model gradient': False,
                #    'plot variance gradient': False,
                #    'plot cost function': False,
                #    'plot objective function': False,
                #    'plot objective gradient': False,
                #    'plot black-box function': False,
                #    'plot as image': False,
                #    'plot density': False,
                #},
                #'q':
                #{
                #    'plot model function': False,
                #    'plot variance function': False,
                #    'plot model gradient': False,
                #    'plot variance gradient': False,
                #    'plot cost function': False,
                #    'plot objective function': False,
                #    'plot objective gradient': False,
                #    'plot black-box function': False,
                #    'plot as image': False,
                #    'plot density': False,
                #},

            }

plot_existing_data = True
plot_data_file = "Data.npy"
RecomputeModelForPlot = True

Animation = {
        'model': 'eta',
        'parameter 1': 'x_position',
        'bounds 1': [-0.8,0.8],
        'parameter 2': 'y_position',
        'bounds 2': [-0.8,0.8]
        }

PlotWithFreshHyperParameters = True
#PlotWithFreshHyperParameters = False
NumberOfSimulatedCurves = 1000
MeanOfSimulatedCurves = np.ones((11))*500
StandardDeviationOfSimulatedCurves = np.ones((11)) * 100
##########################
#####Model Optimization###
##########################
optimization_model_parameter =  ['fraction_vertical']
use_existing_data = False
optimization_data_file = './data/historic_data/...'
FindOptimum = 'maximum'



