##################################
####Surrogate Model Autonomous####
####expeRimenT####################
####SMART#########################
##################################

Welcome toe SMART, an autonomous experiment software developed by Marcus Noack
at Lawrence Berkeley National Lab. After changing the "Config.py" file
just run:

python3.X Run_SMART.py

in a terminal.

To visualize the result type

python3.X Run_Visualization.py

If you are, at any moment, interested in where your model has an optimum
run

python3.X Run_Optimization.py

But before all of that, have a look at the
tutorial by following the link:

https://www.overleaf.com/read/nrcyxxsbhvbx

For questions contact: MarcusNoack@lbl.gov
