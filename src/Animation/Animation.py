import sys
sys.path.insert(0, '../../')
sys.path.insert(1, './src/AutonomusExperimentation')
sys.path.insert(2, '../AutonomousExperimentation/')

import Config as conf
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import ExperimentControls as exp
from scipy.interpolate import interp2d
from matplotlib import cm
import time

path_to_data = "./data/current_data/Data.npy"
data1 = np.load(path_to_data)
data = data1.item()
while len(data) < 10:
    time.sleep(5)
    data = np.load(path_to_data).item()


fig, ax = plt.subplots(sharex=True, figsize=(5,5))
ax.set(xlim=(conf.Animation['bounds 1'][0], conf.Animation['bounds 1'][1]), 
        ylim=(conf.Animation['bounds 2'][0], conf.Animation['bounds 2'][1]))
ax.set_xlabel(conf.Animation['parameter 1'])
ax.set_ylabel(conf.Animation['parameter 2'])

data_array = np.zeros((len(data),3))
for idx in data:
    data_array[idx,0] = data[idx][conf.Animation['parameter 1']]
    data_array[idx,1] = data[idx][conf.Animation['parameter 2']]
    data_array[idx,2] = data[idx][conf.Animation['model']]
x_list = data_array[:,0]
y_list = data_array[:,1]
z_list = data_array[:,2]


myplot = ax.tripcolor(x_list,y_list,z_list,cmap = plt.get_cmap(cm.viridis))

def animate(*args):
    data = np.load(path_to_data).item()
    data_array = np.zeros((len(data),3))
    for idx in data:
        data_array[idx,0] = data[idx][conf.Animation['parameter 1']]
        data_array[idx,1] = data[idx][conf.Animation['parameter 2']]
        data_array[idx,2] = data[idx][conf.Animation['model']]
    x_list = data_array[:,0]
    y_list = data_array[:,1]
    z_list = data_array[:,2]
    #z_list = z_list/max(z_list)
    myplot = ax.tripcolor(x_list,y_list,z_list,shading = 'gouraud',cmap = plt.get_cmap(cm.viridis))
    plt.scatter(x_list,y_list,c = 'black',s=1)
    #plt.savefig("GIF_plot"+str(len(x_list)))

anim = animation.FuncAnimation(
    fig, animate, interval=100)
    
plt.colorbar(myplot)
plt.draw()
plt.show()
