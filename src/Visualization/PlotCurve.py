import sys
sys.path.insert(0, './src/AutonomousExperimentation/')
sys.path.insert(1, '../../')
sys.path.insert(2, '../AutonomousExperimentation/')
import os
import time
from time import gmtime, strftime
import random
import numpy as np
import Config as conf
import ExperimentControls as ec
import Misc as smc
from DataOK import DataOK as ok
from DataGP import DataGP as gp
from Kernel import Kernel as kernel
from Variogram import Variogram as vario
from DomainExpertGP import DomainExpertGP as DEGP
from SurrogateModel import SurrogateModel as SurrogateModel
from MeasurementCosts import Cost
from ExperimentData import ExperimentData
from DomainExpertData import DomainExpertData as DED

#########################################
######Evaluate Result Autonomous Exp#####
#########################################



print("Plotting Specified Slice...")
print("")
print("")
print("")

if len(sys.argv) == 3:
    ParameterSetFile = sys.argv[1]
    ModelFile = sys.argv[2]
elif len(sys.argv) == 1:
    if conf.plot_existing_data == False:
        DataFile = "./data/current_data/Data.npy"
    elif conf.plot_existing_data == True:
        DataFile = conf.plot_data_file
    else:
        print("No decision made what files to use for ploting")

else:
    print("Wrong number of command line parameters given. Give No or TWO command line parameter. Example: ./VisualizeModel.sh Path_to_parameters Path_to_model")
    exit()


DataSet = np.load(DataFile, allow_pickle = True).item()
ParameterNumber=len(conf.Parameters)

print("Length of parameter set: ", len(DataSet))

experiment_data = ExperimentData(False,DataSet)
experiment_costs = Cost(experiment_data)
Kernels = {}
Variograms = {}
GPs = {}
SurrogateModels = {}
expert_data = {}

index = 0
for model_name in list(conf.ModelFunctions):
    GPs[model_name] = {}
    if conf.Engine == "GP":
        if conf.PlotWithFreshHyperParameters:
            Kernels[model_name] = kernel(experiment_data.Points,experiment_data.Models[:,index],\
            experiment_data.Variances[:,index],model_name,experiment_data.bounds, ReadHyperParameters = False)
        else:
            Kernels[model_name] = kernel(experiment_data.Points,experiment_data.Models[:,index],\
            experiment_data.Variances[:,index],model_name,experiment_data.bounds, ReadHyperParameters = True)

        GPs[model_name]['experiment data'] = \
        gp(experiment_data,experiment_data.Models[:,index],\
        Kernels[model_name],model_name,experiment_costs)
    elif conf.Engine == "OK":
        Variograms[model_name] = vario(experiment_data.Points,experiment_data.Models[:,index])
        GPs[model_name]['experiment data'] = \
        ok(experiment_data,experiment_data.Models[:,index],Variograms[model_name],model_name,experiment_costs)
    else:
        print("No Engine specified in configuration file, exit."); exit()

    index = index + 1


####we could create N random samples of the parameter space (normal) and visualize the curves, the color being
####the model value, the user could then adjust the mean and std deviation of the normal distribution.


import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
time = conf.TimeSeries[1]
x = []
point = []
model = []
mean = np.asarray(conf.MeanOfSimulatedCurves)
std  = np.asarray(conf.StandardDeviationOfSimulatedCurves)
size = len(conf.TimeSeries[1])

for model_name in conf.ModelFunctions:
    for i in range(conf.NumberOfSimulatedCurves):
        point.append(np.random.normal(mean,std,size))
        try:
            model.append(GPs[model_name]['experiment data'].EvaluateMean(point[i]))
        except:
            model.append(0.0)


    model_min = np.min(model)
    model_max = np.max(model)
    norm = mpl.colors.Normalize(vmin=model_min, vmax = model_max)
    cmap = mpl.cm.ScalarMappable(norm=norm, cmap=mpl.cm.jet)
    cmap.set_array([])
    for i in range(conf.NumberOfSimulatedCurves):
        plt.plot(time,point[i],c = cmap.to_rgba(model[i]),alpha = ((model[i]-model_min)/(model_max-model_min))**3)
    plt.colorbar(cmap, ticks=[np.min(model),np.max(model)],label = conf.ModelFunctions[model_name]['unit'])
    plt.title(model_name)
    plt.show()
