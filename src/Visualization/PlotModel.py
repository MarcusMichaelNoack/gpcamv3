import sys
sys.path.insert(0, './src/AutonomousExperimentation/')

import Config as conf
import numpy as np
import matplotlib.pyplot as plt
import ExperimentControls as exp
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from scipy.stats import kde


def PlotSurrogateModel2D(Models,experiment_costs,points,models):

    Slice = {}
    x = []
    x_name = []
    index = 0
    plotting_indices = []
    for para1 in list(conf.Plotting_Parameters):
        if conf.Plotting_Parameters[para1]['plot'][0] == False:
            Slice[para1] = conf.Plotting_Parameters[para1]['plot'][1]
            continue
        plotting_indices.append(index)
        x.append(np.linspace(conf.Plotting_Parameters[para1]['bounds'][0],\
        conf.Plotting_Parameters[para1]['bounds'][1],conf.Plotting_Parameters[para1]['size']))
        x_name.append(para1)
        index += 1
    X,Y = np.meshgrid(x[0],x[1])
    x_lim = [min(x[0]),max(x[0])]
    y_lim = [min(x[1]),max(x[1])]
    model_function = np.zeros((np.shape(X)))
    variance = np.zeros((model_function.shape))
    variance_g = np.zeros((model_function.shape))
    model_g = np.zeros((model_function.shape))
    BB = np.zeros((model_function.shape))
    objective_function = np.zeros((model_function.shape))
    costs = np.zeros((model_function.shape))
    fig_number = 0
    for model in conf.Plotting_ModelFunctions:
        if conf.RecomputeModelForPlot == True:
            for i in range(len(X)):
                for j in range(len(X[0])):
                    p = []
                    point = {}
                    point[0] = {}
                    hit = 0
                    index = 0
                    for para in list(conf.Plotting_Parameters):
                        if conf.Plotting_Parameters[para]['plot'][0] == True:
                            if hit == 0: p.append(X[i,j]);point[0][para] = X[i,j];
                            elif hit == 1: p.append(Y[i,j]);point[0][para] = Y[i,j];
                            else: print('More than two dimensions in a plot is not possible?')
                            hit = hit + 1
                        else:
                            p.append(conf.Plotting_Parameters[para]['plot'][1])
                            point[0][para] = conf.Plotting_Parameters[para]['plot'][1]
                        index = index +1

                    if conf.Plotting_ModelFunctions[model]['plot model function'] == True: model_function[i,j],variance[i,j] = Models[model]['experiment data'].EvaluateGP(np.asarray(p))
                    if conf.Plotting_ModelFunctions[model]['plot variance gradient'] == True: variance_g[i,j] = np.linalg.norm(Models[model]['experiment data'].EvaluateErrorGradient(np.asarray(p)))
                    if conf.Plotting_ModelFunctions[model]['plot model gradient'] == True: model_g[i,j] = np.linalg.norm(Models[model]['experiment data'].EvaluateModelGradient(np.asarray(p)))
                    if conf.Plotting_ModelFunctions[model]['plot cost function'] == True: costs[i,j] =\
                    experiment_costs.ComputeLocalCost(p,Models[model].PointData[len(Models[model].PointData)-1])
                    if conf.Plotting_ModelFunctions[model]['plot objective function'] == True: objective_function[i,j],ll = Models[model]['experiment data'].ComputeObjective(np.asarray(p))
                    if conf.Plotting_ModelFunctions[model]['plot black-box function'] == True: ex = exp.BlackBox(point);BB[i,j] = ex[0][model]
            np.save("./data/current_illustrations/model_"+model,model_function)
            np.save("./data/current_illustrations/variance_"+model,variance)
            np.save("./data/current_illustrations/model_gradient_"+model,model_g)
            np.save("./data/current_illustrations/variance_g"+model,variance_g)
            np.save("./data/current_illustrations/costs_"+model,costs)
            np.save("./data/current_illustrations/objective_function_"+model,objective_function)
            np.save("./data/current_illustrations/black_box_function"+model,BB)
        else:
            model_function = np.load("./data/current_illustrations/model_"+model+".npy")
            variance = np.load("./data/current_illustrations/variance_"+model+".npy")
            model_g = np.load("./data/current_illustrations/model_gradient_"+model+".npy")
            variance_g = np.load("./data/current_illustrations/variance_g"+model+".npy")
            costs = np.load("./data/current_illustrations/costs_"+model+".npy")
            objective_function = np.load("./data/current_illustrations/objective_function_"+model+".npy")
            BB = np.load("./data/current_illustrations/black_box_function"+model+".npy")
            ########################################################
        if conf.Plotting_ModelFunctions[model]['plot model function'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model)
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.scatter(points[:,0],points[:,1],models[:,0])
            ax.plot_surface(X,Y,model_function,rstride = 1, cstride = 1, cmap = cm.viridis)
            fig.savefig("./data/current_illustrations/"+model+".pdf", bbox_inches='tight')
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################
        ########################################################
        if conf.Plotting_ModelFunctions[model]['plot variance function'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' variance function')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.plot_surface(X,Y,variance/np.max(variance),rstride = 1, cstride = 1, cmap = cm.viridis)
            fig_number += 1
            #raise Exception('exit')
        #########################################################
        #########################################################
        #########################################################
        ########################################################
        if conf.Plotting_ModelFunctions[model]['plot objective function'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' objective function')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.set_zlabel('error/cost')
            ax.plot_surface(X,Y,objective_function,rstride = 1, cstride = 1, cmap = cm.viridis)
            fig_number += 1

        #########################################################
        #########################################################
        #########################################################
        ########################################################
        if conf.Plotting_ModelFunctions[model]['plot model gradient'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' model function gradient')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.plot_surface(X,Y,model_g,rstride = 1, cstride = 1, cmap = cm.viridis)
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################

        ########################################################
        if conf.Plotting_ModelFunctions[model]['plot variance gradient'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' variance function gradient')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.plot_surface(X,Y,variance_g,rstride = 1, cstride = 1, cmap = cm.viridis)
            #ax.plot_wireframe(X,Y,variance_g,rstride = 5, cstride = 5)
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################


        ########################################################
        if conf.Plotting_ModelFunctions[model]['plot black-box function'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' black box function')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.plot_surface(X,Y,BB,rstride = 1, cstride = 1, cmap = cm.viridis)
            plt.imshow(BB,extent=[-8.5, 8.5,-8.5,8.5])
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################
        if conf.Plotting_ModelFunctions[model]['plot as image'] == True:
            #print(np.shape(model_function))
            plt.figure(fig_number)
            plt.imshow(model_function)
            plt.title(model)
            plt.xlabel(x_name[0])
            plt.ylabel(x_name[1])
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################
        if conf.Plotting_ModelFunctions[model]['plot cost function'] == True:
            fig = plt.figure(fig_number)
            ax = Axes3D(fig)
            ax.set_title(model+' costs')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            ax.set_zlabel('local costs ['+model + ' unit]')
            ax.plot_surface(X,Y,costs,rstride = 1, cstride = 1, cmap = cm.viridis)
            #ax.plot_wireframe(X,Y,variance_g,rstride = 5, cstride = 5)
            fig_number += 1
        #########################################################
        #########################################################
        #########################################################

        if conf.Plotting_ModelFunctions[model]['plot density'] == True:
            #plt.figure(fig_number)
            fig, ax = plt.subplots(ncols =1,nrows =1 ,figsize=(5,5))
            ##density plot still using hard coded indices
            points = points[:,[plotting_indices[0],plotting_indices[1]]]
            XRange = [x_lim[0],x_lim[1]]
            YRange = [y_lim[0],y_lim[1]]
            xi, yi = np.mgrid[XRange[0]:XRange[1]:0.1,\
                              YRange[0]:YRange[1]:0.1]
            k = kde.gaussian_kde(points.T)
            NormArray = np.zeros((100,2))
            counter = 0
            for i in np.linspace(XRange[0],XRange[1],10):
                for j in np.linspace(YRange[0],YRange[1],10):
                    NormArray[counter][0] = i
                    NormArray[counter][1] = j
                    counter = counter + 1
            n = kde.gaussian_kde(NormArray.T)
            zi = k(np.vstack([xi.flatten(), yi.flatten()]))
            ni = n(np.vstack([xi.flatten(), yi.flatten()]))
            zi = (zi/sum(zi))*len(points)
            ni = (ni/sum(ni))*len(points)
            ax.set_title('Density Plot')
            ax.set_xlabel(x_name[0])
            ax.set_ylabel(x_name[1])
            density = zi.reshape(xi.shape) - ni.reshape(xi.shape)
            density = density + abs(np.amin(density))
            density = (len(points)*density)/(sum(sum(density))*(0.1**2))
            print("integral: ",sum(sum(density))*0.1**2)
            m = ax.pcolormesh(xi, yi, density,\
                    shading='gouraud', cmap=plt.cm.gist_ncar)
            fig.colorbar(m)
            fig_number += 1
    plt.show()



##########################################
##########################################
##########################################
##########################################
