
#sudo pip3 install numba
#sudo pip3 install numba icc_rt

enable_numba = True

default_conf = {"fastmath": True, "cache":True}

blank = lambda x : x

try:
    if enable_numba: 
        from numba import *
    else:
        raise Exception

except Exception:

    def njit(*args, **kwargs): return blank
    def jit(*args, **kwargs): return blank
