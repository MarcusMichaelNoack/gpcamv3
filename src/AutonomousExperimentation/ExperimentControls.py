#########################
##Experiment Functions###
#########################


import numpy as np
import Config as conf
import os
import random
import time
import Misc as smc
from scipy.interpolate import griddata
import sys
sys.path.insert(0,"./expert_knowledge/")
from toy import res
#first_call = True

def BlackBox(data):
    global first_call
    if conf.GetDataFrom == 'synthetic':
        for model in conf.ModelFunctions:
            print("model: ",model)
            for idx in data:
                #if data[idx]['measured'] == True: continue
                #############################################################
                #t_curve = [data[idx]['temperature_0'],data[idx]['temperature_6'],data[idx]['temperature_12'],data[idx]['temperature_18'],data[idx]['temperature_24'],data[idx]['temperature_30'],data[idx]['temperature_36'],data[idx]['temperature_42'],data[idx]['temperature_48'],data[idx]['temperature_54'],data[idx]['temperature_60']]
                #a,b,c,d = res(t_curve)
                #data[idx][model] = d[-1]
                #############################################################
                x = data[idx]['x_position']
                x = data[idx]['y_position']
                if x > 0.0: data[idx][model] = np.sin(x/100.0)
                else: data[idx][model] = 0.0
                #############################################################
                #data[idx][model] = np.sin(data[idx]['temperature'])
                #############################################################
                data[idx]['cost'] = 0.0
                data[idx][model+' variance'] = np.random.uniform(0,0.0001)
                data[idx]["measured"] = True
        return data
    elif conf.GetDataFrom == 'experiment':
        if conf.Overlap_mode:
            data = PerformExperimentOverlap(data)
        else:
            data = PerformExperiment(data)
        #first_call = False
        return data
    elif conf.GetDataFrom == 'interpolated':
        data = InterpolateExpData(data,conf.DataFile)
        #first_call = False
        return data
    else:
        print("Not specified if data is experimental or synthetic.");exit()

def PerformExperiment(data):
    path_new_experiment_command = "./data/new_experiment_command/"
    path_new_experiment_result  = "./data/new_experiment_result/"
    if os.path.isfile(path_new_experiment_result+"experiment_result.npy"):
        os.remove(path_new_experiment_result+"experiment_result.npy")
    np.save(path_new_experiment_command+"experiment_command",data)

    S_time = time.time()
    while not os.path.isfile(path_new_experiment_result+"experiment_result.npy"):
        print("waiting for Measurement...")
        time.sleep(conf.wait_for_experiment)
        if time.time() - S_time > conf.Max_Waiting_Time:
            np.save(path_new_experiment_command+"experiment_command",data)
            S_time = time.time()
            print("Next experiment suggested again due to inactivity of experiment!")

    data = np.load(path_new_experiment_result+"experiment_result.npy").item()
    return data

def PerformExperimentOverlap(data):
    path_new_experiment_command = "./data/new_experiment_command/"
    path_new_experiment_result  = "./data/new_experiment_result/"
    while os.path.isfile(path_new_experiment_command+"experiment_command.npy"):
        time.sleep(conf.wait_for_experiment)
        print("Waiting for experiment device to read and subsequently delete last command.")

    read_success = False
    write_success = False
    try:
        new_data = np.load(path_new_experiment_result+"experiment_result.npy",encoding = "ASCII").item()
        data.update(new_data)
        read_success = True
        print("Successfully received updated data set from experiment device")
    except:
        #print("exception: ", sys.exc_info()[0])
        read_success = False

    while write_success == False:
        try:
            np.save(path_new_experiment_command+"experiment_command",data)
            write_success = True
            print("Successfully send data set to experiment device")
        except:
            time.sleep(conf.wait_for_experiment)
            print("Saving new experiment command file not successful, trying again...")
            write_success = False
    while read_success == False:
        try:
            new_data = np.load(path_new_experiment_result+"experiment_result.npy",encoding = "ASCII",allow_pickle = True).item()
            data.update(new_data)
            #os.remove(path_new_experiment_result+"experiment_result.npy")
            read_success = True
            print("Successfully received data set from experiment device")
        except:
            #print("exception: ", sys.exc_info()[0])
            time.sleep(conf.wait_for_experiment)
            print("New measurement results have not been written yet.")
            read_success = False

    return data

def InterpolateExpData(data,File):
    interpolate_data_dict = np.load(File).item()
    interpolate_data_array = np.zeros((len(interpolate_data_dict),len(interpolate_data_dict[0])))
    for idx in range(len(interpolate_data_dict)):
        index = 0
        for para in list(conf.Parameters):
            interpolate_data_array[idx,index] = interpolate_data_dict[idx][para]
            index += 1
        for model in list(conf.ModelFunctions):
            interpolate_data_array[idx,index] = interpolate_data_dict[idx][model]
            index += 1
    p = interpolate_data_array[:,0:len(conf.Parameters)]
    m = interpolate_data_array[:,len(conf.Parameters):len(conf.Parameters)+len(conf.ModelFunctions)]
    for idx in data:
        index = 0
        for model in list(conf.ModelFunctions):
            ParameterSet = [data[idx][x] for x in list(conf.Parameters)]
            Sample = griddata(p,m[:,index],ParameterSet,method='linear',fill_value=0)
            data[idx][model] = Sample[0]
            data[idx][model+' variance'] = 0.0
            data[idx]['cost'] = 1.0
            index = index + 1
        data[idx]['cost'] = 5.0
        data[idx]['variance'] = 0.1
    return data

def FillWithRandom(Array):
    for i in range(len(Array)):
        Array[i] = conf.Parameters[i][1]+\
        (random.random()*abs(conf.Parameters[i][2]-conf.Parameters[i][1]))
    return Array

###########################################################################
###########################################################################
###########################################################################
def FindNextExperiment(data,SurrogateModels):
    Optima = {}
    PropertyList = ParameterProperties()
    for model in conf.ModelFunctions:
        #if conf.ModelFunctions[model]['use for steering'] == False:
        #    continue
        #the FindErrorMaximum function returns a list of optima (1 or more)
        Optima[model] = SurrogateModels[model].FindErrorMaximum('all_elements')

        #print("Optima Model",model,Optima[model])
        ###Snapping
        Optima[model] = data.Snap(Optima[model],'elements')
        if len(ElementOfPrepP(Optima[model],data)) == 0:
            Optima[model] = SurrogateModels[model].FindErrorMaximum('prep_elements')
            ###Snapping
            Optima[model] = data.Snap(Optima[model],'prepared_elements')
    Optima = ResolutionViolation(Optima,data)
    if len(Optima) == 0:
        for model in conf.ModelFunctions:
            print(model)
            Optima[model] = ChooseExperimentFromList(data)
            Optima[model][0]['error_value'] = SurrogateModels[model].EvaluateSurrogateModelFunction(data.DictToPoint(Optima[model][0]))[1]
            break
        print('Optima chosen from existence list')
        print(Optima)
    print("Optima: ",Optima)
    
    #####here we have a dictionary with one or several optima
    #####per model, we do
    #####From that we have to find one or several new experiment locations
    NextExperiments = {}
    index = data.PointNumber
    for model in Optima:
        for idx in Optima[model]:
            NextExperiments[index] = {}
            for para_name in conf.Parameters:
                NextExperiments[index][para_name] = Optima[model][idx][para_name]
            index = index + 1
    return NextExperiments,Optima

def ParameterProperties():
    Properties = []
    for name in conf.Parameters:
        Properties.append(conf.Parameters[name]['property'])
    return Properties
def ElementOfPrepP(Optima,data):
    for idx in list(Optima):
        for name in Optima[idx]:
            if name == 'error_value':
                continue
            if conf.Parameters[name]['property'] == 'connected':
                if Optima[idx][name] < conf.Parameters[name]['prepared_elements'][0] or\
                    Optima[idx][name] > conf.Parameters[name]['prepared_elements'][1]:
                        data.UpdateDemandList(data.DemandList,Optima[idx])
                        del Optima[idx]
                        break
            elif conf.Parameters[name]['property'] == 'disconnected':
                if Optima[idx][name] < np.amin(conf.Parameters[name]['prepared_elements']) or\
                    Optima[idx][name] > np.amax(conf.Parameters[name]['prepared_elements']):
                        data.UpdateDemandList(data.DemandList,Optima[idx])
                        del Optima[idx]
                        break
            elif conf.Parameters[name]['property'] == 'discrete':
                if Optima[idx][name] < np.amin(conf.Parameters[name]['prepared_elements']) or\
                    Optima[idx][name] > np.amax(conf.Parameters[name]['prepared_elements']):
                        if [Optima[idx][name]] in [list(data.ExistenceList[x].values()) for x in data.ExistenceList]:
                            break
                        data.UpdateDemandList(data.DemandList,Optima[idx])
                        del Optima[idx]
                        break
    return Optima

def ResolutionViolation(optima,data):
    for model in optima:
        for idx_optima in list(optima[model]):
            ##############################
            for model2 in optima:
                for idx_optima2 in list(optima[model2]):
                    if idx_optima == idx_optima2 and model == model2: continue
                    if all([abs(optima[model][idx_optima][para_name] -\
                    optima[model2][idx_optima2][para_name]) < \
                    conf.Parameters[para_name]['resolution'] for para_name in conf.Parameters]):
                        del optima[model2][idx_optima2]
                        break
            ##############################
            for idx_points in data.data_set_points:
                if all([abs(optima[model][idx_optima][para_name] - \
                data.data_set_points[idx_points][para_name])\
                < conf.Parameters[para_name]['resolution'] for para_name in conf.Parameters]):
                    del optima[model][idx_optima]
                    break
    for model in list(optima):
        if not list(optima[model]):
            del optima[model]

    return optima
def ChooseExperimentFromList(data):
    optimum = {}
    optimum[0]  ={}
    for idx_el in data.ExistenceList:
        for parameter in conf.Parameters:
            if conf.Parameters[parameter]['property'] == \
            'connected':
                lower_limit = \
                conf.Parameters[parameter]['prepared_elements'][0]
                upper_limit = \
                conf.Parameters[parameter]['prepared_elements'][1]
                optimum[0][parameter] = random.uniform(lower_limit,upper_limit)
            elif conf.Parameters[parameter]['property'] == \
            'discrete':
                length = \
                len(conf.Parameters[parameter]['prepared_elements'])
                optimum[0][parameter] = \
                data.ExistenceList[idx_el][parameter]
            elif conf.Parameters[parameter]['property'] == \
            'disconnected':
                length = \
                len(conf.Parameters[parameter]['prepared_elements'])
                random_number = np.random.randint(0,length-1)
                lower_limit = \
                conf.Parameters[parameter]['prepared_elements']\
                [random_number][0]
                upper_limit = \
                conf.Parameters[parameter]['prepared_elements']\
                [random_number][1]
                optimum[0][parameter] = random.uniform(lower_limit,upper_limit)

    return optimum
