import time
from time import sleep
import random
import numpy as np
import Config as conf
import SimulationControls as sim
from random import seed
from random import randint
import itertools
import Misc as smc
import numba_wrapper as nb

class DomainExpertData:
    def __init__(self, model_name,experiment_data):
        self.ExpertPointData = {}
        self.ParametricData = {}
        self.SimulationData = {}
        self.PointNumber = conf.initial_data_set_size
        self.model_name = model_name
        for expert_model in conf.ModelFunctions[model_name]['expert knowledge']['expert models']:
            self.ExpertPointData[expert_model] = {}
            self.ParametricData[expert_model] = {}
            self.SimulationData[expert_model] = {}
            if conf.ModelFunctions[model_name]['expert knowledge']['expert models'][expert_model]['source'] == 'points':
                self.ExpertPointData[expert_model] = self.MakePointData(model_name,expert_model)
            if conf.ModelFunctions[model_name]['expert knowledge']['expert models'][expert_model]['source']  == 'DomainAwareMean':
                self.ParametricData[expert_model]  = self.DomainAwareMean(model_name,expert_model)
            if conf.ModelFunctions[model_name]\
            ['expert knowledge']['expert models'][expert_model]['source']  == 'simulation':
                self.SimulationData[expert_model]  = self.MakeSimulationData(expert_model,experiment_data)
        print(self.SimulationData[expert_model])
        exit()
    def MakePointData(self,model_name,exp_model_name):
        path = conf.ModelFunctions[model_name]['expert knowledge']['expert models'][exp_model_name]['path']
        expert_dict = np.load(path).item()
        expert_array = np.zeros((len(expert_dict),len(expert_dict[0])))
        for idx in expert_dict:
            index = 0
            for para_name in list(conf.Parameters):
                expert_array[idx,index] = expert_dict[idx][para_name]
                index = index + 1
            expert_array[idx,index]   = expert_dict[idx][model_name]
            expert_array[idx,index+1] = expert_dict[idx]['variance']
        print("    I read domain-expert data based on points.")
        print("    Model Name: ",self.model_name," Expert-Knowledge Basis name: ",exp_model_name," Number of points:", len(expert_array))
        return expert_array

    def MakeSimulationData(self,expert_model,expriment_data):
        self.data_set = experiment_data.data_set
        self.data_set = self.Simulate(self.data_set)
        self.PointNumber = len(self.data_set)
        self.dim = len(conf.Parameters)
        self.bounds = experiment_data.bounds
        self.data_set_points =experiment_data.data_set_points
        self.data_set_models =experiment_data.data_set_models
        self.data_set_variances =experiment_data.data_set_variances
        self.data_set_costs = experiment_data.data_set_costs
        self.Points = experiment_data.Points
        self.Models = experiment_data.Models
        self.Variances = experiment_data.Variances
        #self.MeasurementCosts = self.ExtractCostsFromData()
        return 0
    def Simulate(self,data_set):
        return sco.PerformSimulation(data_set)

    def DomainAwareMean(self):
        print("The use of domain-aware mean is not implemented yet, exit");exit()

    def ExtractBounds(self):
        Bounds = [[],[]]
        for name in list(conf.Parameters):
            if conf.Parameters[name]['property'] == 'connected':
                Bounds[0].append(conf.Parameters[name]['elements'])
                Bounds[1].append(conf.Parameters[name]['prepared_elements'])
            elif  conf.Parameters[name]['property'] == 'disconnected':
                Bounds[0].append([np.min(conf.Parameters[name]['elements']),\
                        np.max(conf.Parameters[name]['elements'])])
                Bounds[1].append([np.min(conf.Parameters[name]['prepared_elements']),\
                        np.max(conf.Parameters[name]['prepared_elements'])])
            elif  conf.Parameters[name]['property'] == 'discrete':
                Bounds[0].append([np.min(conf.Parameters[name]['elements']),\
                        np.max(conf.Parameters[name]['elements'])])
                Bounds[1].append([np.min(conf.Parameters[name]['prepared_elements']),\
                        np.max(conf.Parameters[name]['prepared_elements'])])
        return Bounds

    def SeperateModelAndPoints(self):
        data_set_points = {}
        data_set_models = {}
        data_set_variances = {}
        data_set_costs = {}
        for idx in self.data_set:
            data_set_points[idx] = {}
            data_set_models[idx] = {}
            data_set_variances[idx] = {}
            try: data_set_costs[idx] = self.data_set[idx]['cost']
            except: pass
            for parameter in list(conf.Parameters):
                data_set_points[idx][parameter] = self.data_set[idx][parameter]
            for model_name in list(conf.ModelFunctions):
                data_set_models[idx][model_name] = self.data_set[idx][model_name]
                try: data_set_variances[idx][model_name] = \
                        self.data_set[idx][model_name+' variance']
                except: pass

        return data_set_points,data_set_models, data_set_variances,data_set_costs

    def ExtractPointsFromData(self):
        P = np.zeros((self.PointNumber,self.dim))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.Parameters):
                P[idx,index] = self.data_set[idx][name]
                index += 1
        return P
    def ExtractModelFromData(self):
        M = np.zeros((self.PointNumber,len(conf.ModelFunctions)))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.ModelFunctions):
                M[idx,index] = self.data_set[idx][name]
                index +=1 
        return M

    def ExtractVarianceFromData(self):
        Variance = np.zeros((self.PointNumber,len(conf.ModelFunctions)))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.ModelFunctions):
                Variance[idx,index] = self.data_set[idx][name+' variance']
                index +=1
        return Variance

    def InitializeData(self):
        data = self.CreateRandomData()
        return data

    def CreateRandomData(self):
        data = {}
        point_index = 0
        while len(data) < self.PointNumber:
            data[point_index] = {}
            for parameter in conf.Parameters:
                if conf.Parameters[parameter]['property'] == \
                'connected':
                    lower_limit = \
                    conf.Parameters[parameter]['prepared_elements'][0]
                    upper_limit = \
                    conf.Parameters[parameter]['prepared_elements'][1]
                    data[point_index][parameter] = random.uniform(lower_limit,upper_limit)
                elif conf.Parameters[parameter]['property'] == \
                'discrete':
                    length = \
                    len(conf.Parameters[parameter]['prepared_elements'])
                    data[point_index][parameter] = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [randint(0,length-1)]
                elif conf.Parameters[parameter]['property'] == \
                'disconnected':
                    length = \
                    len(conf.Parameters[parameter]['prepared_elements'])
                    random_number = randint(0,length-1)
                    lower_limit = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [random_number][0]
                    upper_limit = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [random_number][1]
                    data[point_index][parameter] = random.uniform(lower_limit,upper_limit)
                else:
                    print("no valid property given. exit")
                    exit()
            data[point_index]["measured"] = False
            if conf.Costs == True:
                data[point_index]['cost'] = 0.0
            else:
                data[point_index]['cost'] = None
            for model in conf.ModelFunctions:
                if conf.ModelFunctions[model]['expert knowledge']['switch'] == 'off': continue
                data[point_index][model+' simulation variance'] = 0.0
                data[point_index][model+' simulation'] = None

            point_index = len(data)
        data = sim.PerformSimulation(data)
        #print("Simulated data:")
        #self.PrintData(data)
        return data

