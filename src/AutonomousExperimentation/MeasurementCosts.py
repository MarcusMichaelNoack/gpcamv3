################
##Cost Class####
################


import random
import numpy as np
import Config as conf
import Misc as sm

class Cost():
    def __init__(self,Data):
        self.Data = Data
        self.Points = Data.data_set_points
        self.PointNumber = len(self.Points)
        self.Costs = {}
        for name in conf.Parameters:
            self.Costs[name] = [conf.Parameters[name]['costs'][0],conf.Parameters[name]['costs'][1]]
        self.Costs['offset'] = conf.CostOffset
    def Update(self,Data):
        self.Data = Data
        self.Points = self.Data.data_set_points
        self.PointNumber = len(self.Points)
    def ComputeLocalCost(self, p, o):
        if isinstance(p,dict) == False:
            position = {}
            index = 0
            for name in list(conf.Parameters):
                position[name] = p[index]
                index += 1
        else:
            position = dict(p)
        if isinstance(o,dict) == False:
            origin = {}
            index = 0
            for name in list(conf.Parameters):
                origin[name] = o[index]
                index += 1
        else:
            origin = dict(o)

        offset = self.Costs['offset']
        index = 0
        cost = 0
        sum1 = 0
        for name in conf.Parameters:
            if self.Costs[name][0] == 'linear':
                sum1 = self.LinearCost(self.Costs[name][1],\
                abs(position[name]-origin[name]))
            elif self.Costs[name][0] == 'sigmoid':
                #print(name,self.Costs[name][0])
                sum1 = self.SigmoidCost(\
                self.Costs[name][1],\
                conf.Parameters[name]['costs'][2],\
                abs(position[name]-origin[name]))
            cost = cost + sum1
        cost = cost + offset
        return cost
    def ComputeLocalCostGradient(self, p, origin):
        cost_gradient = np.zeros((len(p)))
        position = {}
        if isinstance(p,dict) == False:
            index = 0
            for name in list(conf.Parameters):
                position[name] = p[index]
                index += 1
        else:
            position = p
        offset = self.Costs['offset']
        index = 0
        cost = 0
        sum1 = 0
        for name in list(conf.Parameters):
            if self.Costs[name][0] == 'linear':
                #print(name,self.Costs[name][0])
                sum1 = self.LinearCostDerivative(self.Costs[name][1],\
                abs(position[name] - origin[name]))
            elif self.Costs[name][0] == 'sigmoid':
                #print(name,self.Costs[name][0])
                sum1 = self.SigmoidCostDerivative(\
                self.Costs[name][1],\
                conf.Parameters[name]['costs'][2],\
                abs(position[name]-origin[name]))
            cost_gradient[index] = sum1
            index  = index  + 1
        return cost_gradient
    def ComputeLocalCostHessianDiag(self, p, origin):
        cost_hessian_diag = np.zeros((len(p)))
        position = {}
        if isinstance(p,dict) == False:
            index = 0
            for name in list(conf.Parameters):
                position[name] = p[index]
                index += 1
        else:
            position = p
        offset = self.Costs['offset']
        index = 0
        cost = 0
        sum1 = 0
        for name in list(conf.Parameters):
            if self.Costs[name][0] == 'linear':
                #print(name,self.Costs[name][0])
                sum1 = self.LinearCostDerivative2(self.Costs[name][1],\
                abs(position[name] - origin[name]))
            elif self.Costs[name][0] == 'sigmoid':
                #print(name,self.Costs[name][0])
                sum1 = self.SigmoidCostDerivative2(\
                self.Costs[name][1],\
                conf.Parameters[name]['costs'][2],\
                abs(position[name]-origin[name]))
            cost_hessian_diag[index] = sum1
            index  = index  + 1
        hessian = np.zeros((len(p),len(p)))
        return np.fill_diag(hessian,hessian_diag)

    def AdjustCosts(self):
        Points = self.Data.data_set_points
        Costs = self.Data.data_set_costs
        tmp = dict(Costs)
        if bool(Costs) == False: return 0
        if np.random.randint(10) < 8:
            return 0
        if self.PointNumber < 3 *  len(conf.Parameters):
            return 0
        if conf.Costs  == False or conf.AdjustCosts[0] == False:
            return 0
        print('Cost adjustment in progress...')
        print('Old cost parameters:')
        self.Data.PrintData(Costs)
        c = [Costs[x] for x in Costs]
        mean_costs = np.mean(c)
        #exit()
        sd = np.std(c)
        for idx in list(Costs):
            if Costs[idx] >= mean_costs - 2.0*sd and Costs[idx] <= mean_costs + 2.0*sd:
                continue
            else:
                del Costs[idx]
        epsilon = 1.0
        counter = 0
        while epsilon > 0.00001:
            counter = counter + 1
            Misfit = self.CostMisfit(Costs,Points)
            #print('epsilon: ',epsilon,' Misfit: ', Misfit)
            ###compute gradient
            gradient = {}
            for para in conf.Parameters:
                #loop through the components of the gradient
                gradient[para] = 0
                for idx in Costs:
                    if idx == 0: continue
                    DeltaX = self.ComputeDelta(Points[idx],Points[idx-1])
                    A = self.ComputeLocalCost(Points[idx],Points[idx-1]) - Costs[idx]
                    if self.Costs[para][0] == 'linear':
                        B = self.LinearCost(1.0,DeltaX[para])
                    elif self.Costs[para][0] == 'sigmoid':
                        B = self.SigmoidCost(1.0,\
                        conf.Parameters[para]['costs'][2],DeltaX[para])
                    gradient[para] = gradient[para] + (A*B)
            gradient['offset'] = 0.0
            for idx in Costs:
                if idx == 0: continue
                A = self.ComputeLocalCost(Points[idx],Points[idx-1]) - Costs[idx]
                gradient['offset'] = gradient['offset'] + A
            #####################################
            ###compute hessian for a newton step#
            #####################################
            hessian = {}
            for para1 in gradient:
                hessian[para1] = {}

                for para2 in gradient:
                    hessian[para1][para2] = 0
                    if para1 == 'offset' and para2 == 'offset':
                        continue
                    for idx in Costs:
                        if idx == 0: continue
                        DeltaX = self.ComputeDelta(Points[idx],Points[idx-1])
                        if para1 == 'offset':
                            if self.Costs[para2][0] == 'linear':
                                B2 = self.LinearCost(1.0,DeltaX[para2])
                            elif self.Costs[para2][0] == 'sigmoid':
                                B2 = self.SigmoidCost(1.0,\
                                conf.Parameters[para2]['costs'][2],DeltaX[para2])
                            hessian[para1][para2] = hessian[para1][para2] + (B2)
                        elif para2 == 'offset':
                            if self.Costs[para1][0] == 'linear':
                                B1 = self.LinearCost(1.0,DeltaX[para1])
                            elif self.Costs[para1][0] == 'sigmoid':
                                B1 = self.SigmoidCost(1.0,\
                                conf.Parameters[para1]['costs'][2],DeltaX[para1])

                            hessian[para1][para2] = hessian[para1][para2] + (B1)
                        else:

                            if self.Costs[para1][0] == 'linear':
                                B1 = self.LinearCost(1.0,DeltaX[para1])
                            elif self.Costs[para1][0] == 'sigmoid':
                                B1 = self.SigmoidCost(1.0,\
                                conf.Parameters[para1]['costs'][2],DeltaX[para1])

                            if self.Costs[para2][0] == 'linear':
                                B2 = self.LinearCost(1.0,DeltaX[para2])
                            elif self.Costs[para2][0] == 'sigmoid':
                                B2 = self.SigmoidCost(1.0,\
                                conf.Parameters[para2]['costs'][2],DeltaX[para2])
                            hessian[para1][para2] = hessian[para1][para2] + (B1*B2)

            hessian['offset']['offset'] = len(Costs)
            g = np.zeros((len(gradient)))
            h = np.zeros((len(gradient),len(gradient)))
            index1 = 0
            for name1 in gradient:
                g[index1] = gradient[name1]
                index2 = 0
                for name2 in gradient:
                    h[index1][index2] = hessian[name1][name2]
                    index2 = index2 + 1
                index1 = index1 + 1

            gamma = np.linalg.solve(h,-g)
            index = 0
            s = {}
            for name in gradient:
                s[name]= gamma[index]
                index = index + 1
            self.Costs['offset'] = self.Costs['offset'] + (s['offset'])
            for para in conf.Parameters:
                self.Costs[para][1] = self.Costs[para][1] + (s[para])
            if counter > 100:
                for name in conf.Parameters:
                    self.Costs[name] = [conf.Parameters[name]['costs'][0],conf.Parameters[name]['costs'][1]]
                self.Costs['offset'] = conf.CostOffset
                print("Adjustment of costs not successful")

            epsilon = sm.L1DictNorm(s)
            if counter > 1000:
                self.Costs = dict(tmp)
                print("Cost update did not converge. Costs will stay the same")
                break
        print('New cost parameters:')
        self.Data.PrintData(self.Costs)
        print("Cost adjustment done.")
        #accepted == False
        #a = input("Would you like to accept the new costs?(y/n)")
        #if a == 'y': accepted = True

        return 0
    def SigmoidCost(self,c,shift,x):
        #function computes the function value of a shifted 1/(1+exp(-x)) function
        a = np.exp(-10.0*(x-shift))
        b = np.exp(-10.0*(-x-shift))
        return c*((1.0/(1.0+a)) + (1.0/(1.0+b)))
    def SigmoidCostDerivative(self,c,shift,x):
        #function computes the function value of a shifted 1/(1+exp(-x)) function
        e1 = -10.0*(x-shift)
        e2 = -10.0*(-x-shift)
        a = np.exp(e1)
        b = np.exp(e2)
        return c*(((10.0*e1*a)/((1.0+a)**2)) + ((-10.0*e2*b)/((1.0+b)**2)))
    def SigmoidCostDerivative2(self,c,shift,x):
        #function computes the function value of a shifted 1/(1+exp(-x)) function
        e1 = -10.0*(x-shift)
        e2 = -10.0*(-x-shift)
        a = np.exp(e1)
        b = np.exp(e2)
        de1_dx = -10.0
        de2_dx = 10.0
        da_dx = -10.0*e1*a
        db_dx = 10.0*e2*b
        da2_dx2 = -10.0*((de1_dx*a)+(da_dx*e1))
        db2_dx2 =  10.0*((de2_dx*b)+(db_dx*e2))
        return c*((((2.0)/((1.0+a)**3))*(da_dx**2)) - ((d2a_dx2)/((1.0+a)**2)) + (((2.0)/((1.0+b)**3))*(db_dx**2))-((d2b_dx2)/((1.0+b)**2)))

    def LinearCost(self,c,x):
        return c*x
    def LinearCostDerivative(self,c,x):
        return c
    def LinearCostDerivatice2(self,c,x):
        return 0.0

    def ComputeDelta(self,Point1,Point2):
        Delta = {}
        for name in Point1:
            Delta[name] = abs(Point1[name] - Point2[name])
        return Delta
    def CostMisfit(self,costs,points):
        sum1 = 0.0
        for idx in costs:
            if idx == 0: continue
            sum1 = sum1 + ((self.ComputeLocalCost(points[idx],points[idx-1])-costs[idx])**2)
        return sum1/2.0

