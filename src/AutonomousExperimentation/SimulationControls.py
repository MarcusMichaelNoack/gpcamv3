#########################
##Simulation Functions###
#########################


import numpy as np
import Config as conf
import os
import random
import time
import Misc as smc
from scipy.interpolate import griddata
import sys
sys.path.insert(0,"./expert_knowledge/")
from toy import res
#first_call = True

def PerformSimulation(data):
    if conf.GetSimulationDataFrom == 'synthetic':
        for model in conf.ModelFunctions:
            for idx in data:
                if data[idx]['measured'] == True: continue
                #############################################################
                #t_curve = [data[idx]['temperature_0'],data[idx]['temperature_6'],data[idx]['temperature_12'],data[idx]['temperature_18'],data[idx]['temperature_24'],data[idx]['temperature_30'],data[idx]['temperature_36'],data[idx]['temperature_42'],data[idx]['temperature_48'],data[idx]['temperature_54'],data[idx]['temperature_60']]
                #a,b,c,d = res(t_curve)
                #data[idx][model] = d[-1]
                #############################################################
                x = data[idx]['temperature']
                if x > 10.0: data[idx][model] = 0.5
                else: data[idx][model] = 0.0
                #############################################################
                #data[idx][model] = np.sin(data[idx]['temperature'])
                #############################################################
                data[idx]['cost'] = 0.0
                data[idx][model+' variance'] = np.random.uniform(0,0.0001)
                data[idx]["measured"] = True
        return data
    elif conf.GetSimulationDataFrom == 'simulation':
        if conf.Overlap_mode:
            data = PerformExperimentOverlap(data)
        else:
            data = PerformExperiment(data)
        return data
    else:
        print("Not specified if data is experimental or synthetic.");exit()

def PerformSimulation(data):
    path_new_simulation_command = "./data/new_simulation_command/"
    path_new_simulation_result  = "./data/new_simulation_result/"
    if os.path.isfile(path_new_simulation_result+"simulation_result.npy"):
        os.remove(path_new_simulation_result+"simulation_result.npy")
    np.save(path_new_simulation_command+"simulation_command",data)
    S_time = time.time()
    while not os.path.isfile(path_new_simulation_result+"simulation_result.npy"):
        print("waiting for Measurement...")
        time.sleep(conf.wait_for_simulation)
        if time.time() - S_time > conf.Max_Waiting_Time:
            np.save(path_new_simulation_command+"simulation_command",data)
            S_time = time.time()
            print("Next simulation point suggested again due to inactivity of simulation!")

    data = np.load(path_new_simulation_result+"simulation_result.npy").item()
    return data

def PerformSimulationOverlap(data):
    path_new_simlation_command = "./data/new_simlation_command/"
    path_new_simlation_result  = "./data/new_simlation_result/"
    while os.path.isfile(path_new_simlation_command+"simlation_command.npy"):
        time.sleep(conf.wait_for_simlation)
        print("Waiting for simlation device to read and subsequently delete last command.")

    read_success = False
    write_success = False
    try:
        new_data = np.load(path_new_simlation_result+"simlation_result.npy",encoding = "ASCII").item()
        data.update(new_data)
        read_success = True
        print("Successfully received updated data set from simlation device")
    except:
        #print("exception: ", sys.exc_info()[0])
        read_success = False

    while write_success == False:
        try:
            np.save(path_new_simlation_command+"simlation_command",data)
            write_success = True
            print("Successfully send data set to simlation device")
        except:
            time.sleep(conf.wait_for_simlation)
            print("Saving new simlation command file not successful, trying again...")
            write_success = False
    while read_success == False:
        try:
            new_data = np.load(path_new_simlation_result+"simlation_result.npy",encoding = "ASCII").item()
            data.update(new_data)
            #os.remove(path_new_simlation_result+"simlation_result.npy")
            read_success = True
            print("Successfully received data set from simlation device")
        except:
            #print("exception: ", sys.exc_info()[0])
            time.sleep(conf.wait_for_simlation)
            print("New measurement results have not been written yet.")
            read_success = False

    return data
