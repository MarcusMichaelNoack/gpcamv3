######################
##Data GP    Class####
######################

import random
import numpy as np
import Config as conf
import Misc as smc
from Kernel import *
from Kernel import Kernel as kernel
import ExperimentControls as exp
import itertools
import matplotlib.pyplot as plt
from MeasurementCosts import Cost as costs
import Optimization as opt


class DataGP:
    def __init__(self,Data,Model,Kernel,model):
        #input parameters: Data Object, Model Object, Kernel Object, model name, Costs Object
        self.PointNumber = Data.PointNumber                 ###scalar: number of measurement points
        self.dim = Data.dim                                 ##scalar: dimensions of P
        self.Points = Data.Points                           ###array of points
        self.Model  = Model                                 ###1-d array of model values at the points
        self.HyperParameters = Kernel.HyperParameters       ###Array of hyper parameters
        self.model_name = model
        self.bounds = Data.bounds
        self.AveragePointDistance = Data.AveragePointDistance
        self.K = Kernel.K
        self.K_Inv = Kernel.K_Inv
        self.KernelFunction = Kernel.KernelMaternND
        self.d_KernelFunction_dp = Kernel.d_KernelMaternND_dp
        self.GP_Mean = Kernel.Mean
        self.Gaussians = [self.GP_Mean,self.K,self.K_Inv]

    def UpdateGP(self,Data,Model,Kernel,model,Costs):
        self.PointNumber = Data.PointNumber
        self.Points = Data.Points
        self.Model  = Model
        self.GP_Mean = Kernel.Mean
        self.HyperParameters = Kernel.HyperParameters
        self.AveragePointDistance = Data.AveragePointDistance
        self.K = Kernel.K
        self.K_Inv = Kernel.K_Inv
        self.Gaussians = [self.GP_Mean,self.K,self.K_Inv]

    def EvaluateGP(self,p):
        ####This function conditions the GP to give a mean and a variance
        k  = np.zeros((self.PointNumber))
        for i in range(self.PointNumber):
            k[i] = self.KernelFunction(self.Points[i],p,self.HyperParameters)
        ###self.Gaussians[0][0] will later be the mean at p
        return self.Gaussians[0][0] + (k.dot(self.K_Inv)).dot(self.Model-self.Gaussians[0]),self.EvaluateVariance(p)

    def EvaluateMean(self,p):
        k  = np.zeros((self.PointNumber))
        for i in range(self.PointNumber):
            k[i] = self.KernelFunction(self.Points[i],p,self.HyperParameters)
        return self.Gaussians[0][0] + (k.dot(self.K_Inv)).dot(self.Model-self.Gaussians[0])

    def EvaluateVariance(self,p):
        k = np.zeros((self.PointNumber))
        for i in range(self.PointNumber):
            k[i] = self.KernelFunction(self.Points[i],p,self.HyperParameters)
        return self.KernelFunction(p,p,self.HyperParameters) - (k.dot(self.K_Inv)).dot(k)

    def EvaluateVarianceGradient(self,p,direction):
        gradient = np.zeros((self.dim))
        print("you needed the gradient of the GP variance, which is not implemented yet")
        exit()
        #for i in range(len(gradient)):
        #    for j in range(self.PointNumber):
        #        k[j] = self.d_KernelFunction_dp(self.Points[j],p,self.HyperParameters)
        #    gradien[i] = 

        return gradient

    def EvaluateMeanGradient(self,p):
        k = np.zeros((self.PointNumber))
        gradient = np.zeros((self.dim))
        for i in range(len(gradient)):
            for j in range(self.PointNumber):
                k[j] = self.d_KernelFunction_dp(self.Points[j],p,self.HyperParameters,i)
            gradient[i] = (k.dot(self.K_Inv)).dot(self.Model-self.Gaussians[0])
        return gradient

    def EvaluateVarianceHessian(self,p):
        ##########################
        #####Change here to GP####
        ##########################
        hessian = np.zeros((self.dim,self.dim))
        return hessian

    def EvaluateMeanHessian(self,p):
        ##########################
        #####Change here to GP####
        ##########################
        hessian = np.zeros((self.dim,self.dim))
        return hessian

###########################################################################
###########################################################################
###########################################################################
###########################################################################
###########################################################################
###########################################################################

