import random
import numpy as np
import Config as conf
import numba_wrapper as nb


def Dict2D_2_nparray(a):
    return np.array([ list(x.values()) for x in list(a.values()) ])

def RandomNumber(a,b):
    return min(a,b) + (random.random()*abs(b-a))

def MakeDistribution(Array):
    Array = Array-min(Array)
    if max(Array)>0:Array = Array/max(Array)
    if max(Array) == 0:
        print(Array)
        print("Array in MakeDistribution is 0")
        #exit()
    y = np.zeros((100))
    x = np.linspace(0,1,100)
    for i in range(len(Array)):
        y = y + (GaussianBell(x,Array[i],100.0))
    y=y/(sum(y)*0.01)
    return y

def GaussianBell(x,pos,w):
    return np.exp(-w*(x-pos)**2)

def NormedGaussianFunction(x,mean,sigma2):
    return (1.0/np.sqrt(2.0*np.pi*sigma2))*np.exp(-((x-mean)**2)/(2.0*sigma2))

def AddToDiag(Matrix,Vector):
    d = np.einsum('ii->i',Matrix)
    d += Vector
    return Matrix 

#@nb.njit(**nb.default_conf)
def L2Norm(vec1,vec2):
    return np.linalg.norm(np.subtract(vec1,vec2))

def IsElementOf(point,space):
    InSpace = True
    for i in range(len(point)):
        if point[i] < space[i][0] or point[i] > space[i][1]:
            InSpace = False
    return InSpace

def PointHitBoundary(point,space):
    space = np.asarray(space)
    for i in range(len(point)):
        if abs(point[i] - space[i,0]) < 1e-6 or abs(point[i] - space[i,1]) < 1e-6:
            return True
    return False

def bump_function(radius,position,OptimaList):
    sum1 = 0.0
    if len(OptimaList) == 0: return 0.0
    for i in range(len(OptimaList)):
        multi1 = 1.0
        if np.linalg.norm(np.subtract(OptimaList[i,0:len(position)],position)) < radius:
            for j in range(len(position)):
                multi1 = multi1 * (np.exp(-1.0/(radius**2-(position[j]-OptimaList[i,j])**2))/\
                                    np.exp(-1.0/(radius**2)))
            sum1 = sum1 + multi1
        else:
            sum1 = sum1 + 0.0
    return sum1

@nb.njit(**nb.default_conf)
def KroneckerDelta(a,b):
    if a==b:
        result = 1.0
    if a != b:
        result = 0.0
    return result

def KroneckerDelta3D(a,b,c):
    if a==b and b == c:
        result = 1.0
    else:
        result = 0.0
    return result

###################################################
###################################################
def EvaluateAllDataGPs(Models,Points,data):
    for idx in Points:
        point = [Points[idx][x] for x in list(conf.Parameters)]
        for name in conf.ModelFunctions:
            #if conf.ModelFunctions[name]['use for steering'] == False: continue
            Points[idx][name] = Models[name]['experiment data'].EvaluateMean(np.array(point))
    return Points
###################################################
###################################################
def UpdateAllExpertGPs(GPs,experiment_data,domain_expert_data):
    index = 0
    for name in list(conf.ModelFunctions):
        if conf.ModelFunctions[name]['expert knowledge']['switch'] == 'on':
            GPs[name]['expert knowledge'].UpdateExpertGP(domain_expert_data[name],experiment_data)
        index = index + 1
###################################################
###################################################
def UpdateAllDataGPs(data,Kernels,GPs,costs):
    index = 0
    for name in list(conf.ModelFunctions):
        #if conf.ModelFunctions[name]['use for steering'] == False: continue
        GPs[name]["experiment data"].UpdateGP(data,data.Models[:,index],Kernels[name],name,costs)
def UpdateAllSurrogateModels(SurrogateModels,Models,Data,costs):
    index = 0
    for name in list(conf.ModelFunctions):
        #if conf.ModelFunctions[name]['use for steering'] == False: continue
        SurrogateModels[name].update(Models[name],Data,costs)
        index = index + 1
###################################################
###################################################
def UpdateAllVariograms(data,Variograms,mode):
    index = 0
    for name in list(conf.ModelFunctions):
        Variograms[name].UpdateVariogram(data.Points,data.Models[:,index],mode)
        index  = index + 1
def UpdateAllKernels(data,Kernels,mode):
    index = 0
    for name in list(conf.ModelFunctions):
        #if conf.ModelFunctions[name]['use for steering'] == False: continue
        Kernels[name].UpdateKernel(data.Points,data.Models[:,index],\
        data.Variances[:,index],mode)
        index = index + 1
###################################################
###################################################

def L1DictNorm(Dict):
    norm = 0
    for entry in Dict:
        norm = norm + abs(Dict[entry])
    return norm

#@nb.njit(fastmath=True, cache=True)
def MonteCarloIntegration(LowerBounds,UpperBounds,Func,*argv):
    #print("in monte carlo")
    volume = 1.0
    for i in range(len(LowerBounds)):
        volume = volume * abs(UpperBounds[i]-LowerBounds[i])
    #b = np.zeros((0))
    integral = 1.0
    counter = 0
    mean_1 = 0.0
    s_1 = 0.0
    error = 1e6
    while abs(error/integral) > 0.01:
        a = np.random.uniform(LowerBounds,UpperBounds,len(LowerBounds))
        f_eval = Func(a,*argv)
        counter = counter + 1
        mean = mean_1 + ((f_eval - mean_1)/counter)
        s = s_1 + ((f_eval-mean_1)*(f_eval-mean))
        var = s/counter
        error = volume * np.sqrt(var/counter)
        integral = volume * mean
        if counter > 1e6: print("Finding the mc-integral is taking a little longer:",integral,error,abs(error/integral),counter); break
        mean_1 = mean
        s_1 = s
        if counter < 1000: error = 1e6

    #print("Monte Carlo Integration concluded. integral: ",integral,' std: ',std,' percent: ',abs(std/integral),' iterations: ',len(b))
    return integral,error

def KL_Divergence(m1,m2,C1,C1_Inv,C2,C2_Inv):
    return 0.5*(np.trace(C1_Inv.dot(C2))*(m2-m1).dot(C2_Inv).dot(m2-m1)-len(m1)+np.log(np.det(C2)/np.det(C1)))

def KL_DivergenceGradient():
    return 0

def KL_DivergenceHessian():
    return 0

def SimulatePrediction(Prediction,experiment_data,experiment_costs,GPs):
    counter = 0
    for idx in Prediction:
        for model_name in conf.ModelFunctions:
            Prediction[idx][model_name+" variance"] = GPs[model_name]['experiment data'].\
            EvaluateVariance(experiment_data.DictToPoint(Prediction[idx]))
        if counter == 0:
            last_point = experiment_data.Points[-1]
        else:
            last_point = experiment_data.DictToPoint(Prediction[idx-1])
        counter = counter + 1
        if conf.Costs == True:
            Prediction[idx]["cost"] = \
            experiment_costs.ComputeLocalCost(experiment_data.DictToPoint(Prediction[idx]), last_point)
        else:
            Prediction[idx]["cost"] = None
        Prediction[idx]["measured"] = False
    return Prediction

