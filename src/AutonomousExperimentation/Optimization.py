#################
##Model Class####
#################

import random
import numpy as np
import Config as conf
import Misc as smc
#from Kernel import *
#from Kernel import Kernel as kernel
#import ExperimentControls as exp
import itertools
import matplotlib.pyplot as plt
#from MeasurementCosts import Cost as costs

def GradientDescent(StartingPoint,ParameterLimits,ObjectiveFunction,GradientFunction, mode = 1, start = 'x0'):
    ####maximization mode = 1
    ####minimization mode = -1
    Population = InitPopulation(ParameterLimits,1,len(ParameterLimits))
    dim = len(ParameterLimits[0])
    epsilon = np.inf
    step_counter = 0
    step = 1.0
    beta = 0.8
    if start == 'x0':
        x = StartingPoint
    else:
        x = np.copy(Population[0])
        StartingPoint = x
    while epsilon > 0.1:
        step_counter += 1
        gradient = GradientFunction(x)
        while OutOfBounds(x+(step*gradient),ParameterLimits):
            step = 0.5 * step
        if mode == 1:
            while ObjectiveFunction(x+(step*gradient))[0] < ObjectiveFunction(x)[0] + ((step/2.0)*np.linalg.norm(gradient)**2): 
                step = step * beta
        if mode == -1:
            while ObjectiveFunction(x-(step*gradient))[0] > ObjectiveFunction(x)[0] - ((step/2.0)*np.linalg.norm(gradient)**2): 
                step = step * beta
        #print("Gradient at ",x," = ",gradient)
        if OutOfBounds(x + (mode*mode*step*gradient),ParameterLimits) == True: break
        x = x + (mode*mode*step*gradient)
        epsilon = np.linalg.norm(step*gradient)
        #print("gradient",gradient)
        #print("Gradient Descent Position:", x," epsilon: ",epsilon)
        #print("Objective:", ObjectiveFunction(x))
        #print("---------------")
        if step_counter > 50:
            break
    if any(x) < 0: print("x smaller 0"); exit()
    return x, ObjectiveFunction(x), True


def MultiStartGradientDescent(ParameterLimits,ObjectiveFunction,GradientFunction, mode = 1, starts = 10):
    ####maximization mode = 1
    ####minimization mode = -1
    Population = InitPopulation(ParameterLimits,starts,len(ParameterLimits))
    dim = len(ParameterLimits[0])
    OptimaList = np.zeros((starts,dim))
    for i in range(0,starts):
        epsilon = np.inf
        x = np.copy(Population[i])
        step_counter = 0
        step = 1.0
        while epsilon > 1e-8:
            newton_step_counter += 1
            gradient = GradientFunction(x)
            x = x + mode*(gamma + step)
            epsilon = np.linalg.norm(gamma)
        OptimaList[i] = np.copy(x)
        Evaluations[i] = ObjectiveFunction(x)
        max_index = np.argmax(Evaluations)
    return OptimaList[max_index],Evaluations[max_index]

def CG(fun,x0,ParameterLimits):
    from scipy import optimize as o
    pos = o.minimize(fun,x0,method = "Powell")
    return pos,fun(pos)

def Newton(StartingPoint,ParameterLimits,ObjectiveFunction,GradientFunction,HessianFunction, mode = -1,start = 'random'):
    ####maximization mode = 1
    ####minimization mode = -1
    Population = InitPopulation(ParameterLimits,1,len(ParameterLimits))
    dim = len(ParameterLimits[0])
    epsilon = np.inf
    if start == 'x0':
        x = StartingPoint
    else:
        x = np.copy(Population[0])
        StartingPoint = x
    newton_step_counter = 0
    success = True
    print("starting at ",x)
    while epsilon > 1e-5:
        newton_step_counter += 1
        #print("COUNTER:")
        #print(newton_step_counter)
        gradient = GradientFunction(x)
        hessian = HessianFunction(x)
        #print("Gradient: ")
        #print(gradient)
        #print("Hessian:")
        #print(hessian)
        gamma = np.linalg.solve(hessian,-1.0*gradient)
        x = x + (gamma)
        print("position: ",x)
        #print("step:")
        #print(gamma)
        eigen_values,eigen_vectors = np.linalg.eig(hessian)
        #print("eigenvalues:",eigen_values)
        #print("-------------------")
        if OutOfBounds(x,ParameterLimits) == True:
            x = InitPopulation(ParameterLimits,1,len(ParameterLimits))[0]
            #x = StartingPoint + np.random.uniform(-1,1,len(x))
            return x, ObjectiveFunction(x),success
            print("x out of bounds, changed to: ",x)
            epsilon = np.inf
        #input()
        epsilon = np.linalg.norm(gradient)
        print("epsilon:", epsilon," at ", ObjectiveFunction(x))
        if epsilon < 1e-5:
            #eigen_values,eigen_vectors = np.linalg.eig(hessian)
            if any(eigen_values) > 0.0 and mode == 1:
                x = InitPopulation(ParameterLimits,1,len(ParameterLimits))
                epsilon = np.inf
            if any(eigen_values) < 0.0 and mode == -1:
                x = InitPopulation(ParameterLimits,1,len(ParameterLimits))
                epsilon = np.inf
        if all(eigen_values) < 1e-6:
            x = InitPopulation(ParameterLimits,1,len(ParameterLimits))
            epsilon = np.inf
        if newton_step_counter > 2000:
            print("Newton not successful, returning starting point")
            success = False
            x = StartingPoint
            break

    print("Newton DONE:")
    print(x)
    print(ObjectiveFunction(x))
    return x, ObjectiveFunction(x),success

def MultiStartNewton(ParameterLimits,ObjectiveFunction,GradientFunction,HessianFunction, mode = 1, starts = 10):
    ####maximization mode = 1
    ####minimization mode = -1
    Population = InitPopulation(ParameterLimits,starts,len(ParameterLimits))
    dim = len(ParameterLimits[0])
    OptimaList = np.zeros((starts,dim))
    for i in range(0,starts):
        epsilon = np.inf
        x = np.copy(Population[i])
        step_counter = 0
        step = 1.0
        while epsilon > 1e-8:
            newton_step_counter += 1
            gradient = GradientFunction(x)
            hessian = HessianFunction(x)
            gamma = np.linalg.solve(hessian,-gradient)
            x = x + gamma
            epsilon = np.linalg.norm(gamma)

        OptimaList[i] = np.copy(x)
        Evaluations[i] = ObjectiveFunction(x)
        if mode == 1: index = np.argmax(Evaluations)
        if mode == -1:index = np.argmin(Evaluations)
    return OptimaList[index],Evaluations[index]


def HGDN(ParameterLimits,ObjectiveFunction,GradientFunction,HessianFunction):
    #hybrid genetic deflated Newton
    dim = len(ParameterLimits[0])
    OptimaList = np.zeros((0,dim))
    Population = InitPopulation(ParameterLimits,conf.population_size,len(ParameterLimits))
    PopulationObjectiveEvaluation = ComputePopulationObjective(Population,ObjectiveFunction)
    Objective = PopulationObjectiveEvaluation[:,0]
    last_objective = max(Objective)
    e = np.inf
    while e > 0.001:
        Population = Recombination(Population,Objective,ParameterLimits)
        OptimaList, FuncEvals = DNewton(Population,OptimaList,ParameterLimits,ObjectiveFunction,GradientFunction,HessianFunction)
        Population[0:min(len(Population),len(OptimaList))] = np.copy(OptimaList[0:min(len(Population),len(OptimaList))])
        e = FuncEvals[0,0] - last_objective
        last_objective = FuncEvals[0,0]
    return OptimaList[0:min(conf.OptimaList_Length,len(OptimaList))],FuncEvals[0:min(conf.OptimaList_Length,len(OptimaList))]

def DNewton(Population,OptimaList,ParameterLimits,ObjectiveFunction,GradientFunction, HessianFunction):
    #runs a deflated Newton algorithm for all individuals
    #until most individuals diverge
    OldPopulation = np.copy(Population)
    Break = False
    dim = len(ParameterLimits[0])
    while True:
        NotConvergedCounter = 0
        for i in range(len(Population)):
            epsilon = np.inf
            x = np.copy(Population[i])
            newton_step_counter = 0
            step = 1.0
            while epsilon > 1e-8:
                newton_step_counter += 1
                gradient = GradientFunction(x)
                gradient = DeflateGradient(x,gradient,OptimaList)
                hessian = HessianFunction(x)
                gamma = np.linalg.solve(hessian,gradient)
                if smc.IsElementOf(x+(gamma*step),ParameterLimits) == False:
                    step = step/2.0
                    continue
                else:
                    x = x + (gamma*step)
                    epsilon = np.linalg.norm(gamma*step)
                    step = 1.0
                if newton_step_counter > 100 or smc.PointHitBoundary(x,ParameterLimits):
                    NotConvergedCounter += 1
                    break
            OptimaList = np.vstack([OptimaList,x])
            #if NotConvergedCounter > len(Population)/2:
                #return OptimaList
        #Population = np.copy(OldPopulation)
        break

    #[Objective,Error] = ComputePopulationObjective(OptimaList[:,:-1], ObjectiveFunction)
    PopulationObjectiveEvaluation = ComputePopulationObjective(OptimaList[:,:-1], ObjectiveFunction)
    OptimaList = np.flip(OptimaList[PopulationObjectiveEvaluation[:,0].argsort()],axis = 0)
    PopulationObjectiveEvaluation = np.flip(PopulationObjectiveEvaluation[PopulationObjectiveEvaluation[:,0].argsort()],axis = 0)
    return OptimaList,PopulationObjectiveEvaluation

def DeflateGradient(position,gradient,OptimaList):
    #the name says it all
    radius = 1.0
    for i in range(len(gradient)):
        gradient[i] = \
        gradient[i]/(1.0-smc.bump_function(radius,position,OptimaList))
    return gradient


def PerformEvolutionOptimization(ParameterLimits,ObjectiveFunction,index = 0,*args):
    #performs a genetic algorithm
    ######make sure that the array of function evals that comes back from the objective function is a 2d array, so
    ######naturally your objective function only gives back a scalar, make sure you return [result]
    Population = InitPopulation(ParameterLimits,conf.population_size,len(ParameterLimits))
    PopulationObjectiveEvaluation = ComputePopulationObjective(Population,ObjectiveFunction,*args)
    Objective = PopulationObjectiveEvaluation[:,index]
    e = np.inf
    last_objective = -np.inf
    while e > 0.1:
        Population = Recombination(Population,Objective,ParameterLimits)
        PopulationObjectiveEvaluation = ComputePopulationObjective(Population,ObjectiveFunction,*args)
        Objective = PopulationObjectiveEvaluation[:,index]
        e = abs(max(Objective)-last_objective)
        last_objective = max(Objective)
        ###############
        MaxIndex = np.argmax(Objective)
        #print(e,'  ',last_objective,"   ",Population[MaxIndex])
        ######################
    MaxIndex = np.argmax(Objective)
    #print("Final Return of Evo Comp: ",Population[MaxIndex],PopulationObjectiveEvaluation[MaxIndex])
    return Population[MaxIndex],PopulationObjectiveEvaluation[MaxIndex]

def InitPopulation(bounds,number_of_individuals, length_of_genome):
    #function returns a set of randomly chosen points in the Parameter space
    Population = np.zeros((number_of_individuals,length_of_genome))
    for j in range(number_of_individuals):
        for k in range(length_of_genome):
            Population[j,k]=bounds[k][0]+\
            (random.random()*abs(bounds[k][1] - bounds[k][0]))
    return Population

def Recombination(Population,Objective,ParameterLimits):
    #this function takes parent 
    #individuals and creates offspring
    #based on the objective
    #print(Population)
    #print(Objective)
    Offspring=np.zeros((len(Population),len(Population[0])))

    for i in range(0, len(Population)):
        [MaxIndex1,MaxIndex2] = FindMomAndDad(Objective)

        for j in range(0,len(Population[0])):
            a=random.random()
            if a > 0.5:
                Offspring[i,j]=Population[MaxIndex1,j];
            elif a <=0.5:
                Offspring[i,j]=Population[MaxIndex2,j];
            Offspring[i][j] = Mutation(Offspring[i,j],
                    Population[MaxIndex1,j],
                    Population[MaxIndex2,j],j,ParameterLimits)
    return Offspring

def FindMomAndDad(Objective):
    ##based on the objective function value of all individuals, this function
    ##finds a mommy and daddy individual
    b = max(Objective-min(Objective))
    if b == 0:
        b = 0.0001;
    Probability  =  ((Objective-min(Objective))/b)
    RandomNumber = 0.1 +((0.5)*random.random())
    DadFound = False
    counter = 0
    while DadFound == False:
        counter = counter + 1
        SuggestedDad = np.random.random_integers(0,len(Objective)-1)
        if Probability[SuggestedDad] > RandomNumber:
            Dad = SuggestedDad; DadFound = True;
        if counter > 100:
            Dad = SuggestedDad; DadFound = True;
            #print("It took a long time to find a dad, something might be wrong.")

    RandomNumber = 0.1 +((0.5)*random.random())
    MomFound = False
    counter = 0
    while MomFound == False:
        SuggestedMom = np.random.random_integers(0,len(Objective)-1)
        counter = counter +1
        if Probability[SuggestedMom] > RandomNumber and SuggestedMom != Dad:
            Mom = SuggestedMom; MomFound = True;
        if counter > 100:
            Mom = SuggestedMom; MomFound = True;
            #print("It took a long time to find a mom, something might be wrong.")
            #print(Probability)
            #print(RandomNumber)
            #input()

    return Dad,Mom

def Mutation(Offspring,Mom,Dad,index,ParameterLimits):
    random_number = random.random()
    distance = abs(Mom-Dad)/2.0
    mut = -distance+(random_number*2.0*distance)
    NewOffspring = Offspring + mut
    NewOffspring = max((NewOffspring,ParameterLimits[index][0]))
    NewOffspring = min((NewOffspring,ParameterLimits[index][1]))
    random_number = random.random()
    if random_number < 0.01:
        random_number2 = random.random()
        NewOffspring = ParameterLimits[index][0]+\
        (random_number2*(ParameterLimits[index][1]-ParameterLimits[index][0]))
    return NewOffspring

def ComputePopulationObjective(Population,ObjectiveFunction,*args):
    ObjectiveFunctionEvaluation = [0.0] * len(Population)
    for i in range(len(Population)):
        ObjectiveFunctionEvaluation[i] = ObjectiveFunction(Population[i],*args)
    return np.asarray(ObjectiveFunctionEvaluation)


def OutOfBounds(x,bounds):
    for i in range(len(x)):
        if x[i] < bounds[i][0] or x[i] > bounds[i][1]:
            return True
    return False


