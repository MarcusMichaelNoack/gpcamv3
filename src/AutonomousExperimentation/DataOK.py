###############################
##Data   OK Class##############
###############################

import random
import numpy as np
import Config as conf
import Misc as smc
from Variogram import *
import ExperimentControls as exp
import itertools
import matplotlib.pyplot as plt
from MeasurementCosts import Cost as costs
import Optimization as opt

#######This class build and evaluates the Gaussian Process defined by Kriging
#######It provides the mean of the model and the variance, which can then be used
#######in an optimization (not included in this file) to find the next best measurement
#######Also, this class contains the computation of the gradient of mean and variance function

#First parameter = scalar, second parameter = 2D 
@nb.njit(**nb.default_conf)
def Inner_scalar(a, b):
    return np.multiply(a, b)

#First parameter = 2D, second parameter = 2D 
@nb.njit(**nb.default_conf)
def Inner_2D(a, b):
    return np.sum(np.multiply(a, b))

@nb.njit(**nb.default_conf)
def dw_dp(p,n, PointNumber, C_Inv, Points, V):
    #computes the derivatives of all weights in a certain 
    #direction p. Therefore the return parameter is
    dw = np.dot(C_Inv, dD_dp(p, n, PointNumber, Points, V) - dl_dp(p, n, PointNumber, C_Inv, Points, V))
    return dw

@nb.njit(**nb.default_conf)
def d2w_dp2(p,n1,n2, PointNumber, C_Inv, Points, V):
    #computes the derivatives of all weights in a certain 
    #direction p. Therefore the return parameter is
    dw = np.dot(C_Inv, d2D_dp2(p,n1,n2,PointNumber,Points, V )-d2l_dp2(p, n1, n2, PointNumber, C_Inv, Points, V))
    return dw

@nb.njit(**nb.default_conf)
def dD_dp(p, n, PointNumber, Points, V):
    return -dgamma_dh(p, PointNumber, Points, V) * dh_dp(p,n, PointNumber, Points)

@nb.njit(**nb.default_conf)
def d2D_dp2(p, n1, n2, PointNumber, Points, V):
    return -((d2gamma_dh2(p, PointNumber, Points, V)*dh_dp(p,n1, PointNumber, Points))+\
         (dgamma_dh(p, PointNumber, Points, V) * d2h_dp2(p,n1,n2, PointNumber, Points)))

@nb.njit(**nb.default_conf)
def dl_dp(p, n, PointNumber, C_Inv, Points, V):
    One=np.ones(PointNumber)
    C_Aux1 = (np.dot(np.dot(One, C_Inv),One))
    dl_dp = np.dot(np.dot(dD_dp(p, n, PointNumber, Points, V), C_Inv),One)/C_Aux1
    return dl_dp

@nb.njit(**nb.default_conf)
def d2l_dp2(p, n1, n2, PointNumber, C_Inv, Points, V):
    One=np.ones(PointNumber)
    C_Aux1 = (np.dot(np.dot(One,C_Inv),One))
    dl = np.dot(np.dot(d2D_dp2(p, n1, n2, PointNumber, Points, V), C_Inv),One)/C_Aux1
    return dl

@nb.njit(**nb.default_conf)
def dgamma_dh(p, PointNumber, Points, V):
    x = np.zeros(PointNumber)
    for i in range(PointNumber):
        x[i] = np.linalg.norm(np.subtract(Points[i],p))
    return VarioDerivative(x, V[0], V[1])

@nb.njit(**nb.default_conf)
def d2gamma_dh2(p, PointNumber, Points, V):
    x = np.zeros(PointNumber)
    for i in range(PointNumber):
        x[i] = np.linalg.norm(np.subtract(Points[i],p))
    return VarioHessian(x, V[0], V[1])

@nb.njit(**nb.default_conf)    
def dh_dp(p, n, PointNumber, Points):
    Derivative = np.zeros(PointNumber)
    for i in range(PointNumber):
        if smc.L2Norm(p, Points[i]) == 0:
            Derivative[i] = 0
        else:
            Derivative[i] = (p[n]- Points[i,n])/\
                    smc.L2Norm(p, Points[i])
    return Derivative

@nb.njit(**nb.default_conf)
def d2h_dp2(p, n1, n2, PointNumber, Points):
    Derivative = np.zeros(PointNumber)
    for i in range(PointNumber):
        if smc.L2Norm(p, Points[i]) == 0:
            Derivative[i] = 0
        else:
            Derivative[i] = \
            (smc.KroneckerDelta(n1,n2)/smc.L2Norm(p,Points[i]))-\
            (((p[n2]-Points[i,n2])*(p[n1]-Points[i,n1]))/\
            (smc.L2Norm(p, Points[i])**3))
    return Derivative

@nb.njit(fastmath=True, cache=True)
def EvaluateVariance(p, PointNumber, Points, C_Inv, V, C):
    Variance = np.zeros((PointNumber))
    w=np.zeros((PointNumber))
    One=np.ones((PointNumber))
    C_Aux1 = np.dot(np.dot(One,C_Inv),One)
    for j in range(PointNumber):
        Dist=smc.L2Norm(Points[j], p)
        Variance[j]=V[0]- Vario(Dist, V[0], V[1])

    Lambda=(np.dot(np.dot(\
            Variance,C_Inv),One)-1.0)/C_Aux1
    w = np.dot(C_Inv,\
            Variance-Inner_scalar(Lambda,One))
    Error = C[0,0]-Inner_2D(w,Variance)-Lambda
    return Error,w

@nb.njit(fastmath=True, cache=True)
def ComputeWeights(p, PointNumber, Points, C_Inv, V, C):
    Variance = np.zeros((PointNumber))
    w=np.zeros((PointNumber))
    One=np.ones((PointNumber))
    C_Aux1 = np.dot(np.dot(One,C_Inv),One)
    for j in range(PointNumber):
        Dist=smc.L2Norm(Points[j], p)
        Variance[j] = V[0]- Vario(Dist, V[0], V[1])
    Lambda=(np.dot(np.dot(\
            Variance,C_Inv),One)-1.0)/C_Aux1
    w = np.dot(C_Inv,\
            Variance-Inner_scalar(Lambda,One))

    return w



class DataOK:
    def __init__(self,Data,Model,variogram,model,costs):
        self.PointNumber = Data.PointNumber
        self.dim = Data.dim
        self.Points = Data.Points
        self.Model  = Model
        self.V = variogram.VariogramCoefficients
        self.C = variogram.C
        self.C_Inv = variogram.C_Inv
        self.model_name = model
        self.bounds = Data.bounds
        self.Data = Data
        self.experiment_costs = costs
        self.AveragePointDistance = Data.AveragePointDistance

    def UpdateGP(self,Data,Model,variogram,model,costs):
        self.PointNumber = Data.PointNumber
        self.Points = Data.Points
        self.Model = Model
        self.V = variogram.VariogramCoefficients
        self.C,self.C_Inv = ComputeCovariance(self.Points, self.V, len(self.Points))
        self.Data = Data
        self.AveragePointDistance = Data.AveragePointDistance
        self.experiment_costs = costs

    def EvaluateVariance(self,p):
        error, weights = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
        return error

    def EvaluateGP(self,p):
         ####THIS is the most important function in this class.
         ####It conditions the GP to give a mean and a variance
         e,w = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
         m = sum(w*self.Model)
         return m,e

    def EvaluateMean(self,p):
         w = ComputeWeights(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
         m = sum(w*self.Model)
         return m

    def EvaluateVarianceGradient(self,p):
        Variance = np.zeros((self.PointNumber))
        for j in range(self.PointNumber):
            Dist=smc.L2Norm(self.Points[j],p)
            Variance[j]=self.V[0]- Vario(Dist, self.V[0], self.V[1])

        e,w = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
        gradient = np.zeros((self.dim))
        for i in range(self.dim): #loop through dimensions
            A = dw_dp(p,i, self.PointNumber, self.C_Inv, self.Points, self.V)
            B = dD_dp(p,i, self.PointNumber, self.Points, self.V)
            for j in range(self.PointNumber):
                gradient[i] = gradient[i] + ((A[j]*Variance[j])+(w[j]*B[j]))
            gradient[i] = -gradient[i] - dl_dp(p,i, self.PointNumber, self.C_Inv, self.Points, self.V) 
        return gradient

    def EvaluateMeanGradient(self,p):
        gradient = np.zeros((self.dim))
        for i in range(self.dim):
            dw = dw_dp(p,i, self.PointNumber, self.C_Inv, self.Points, self.V)
            for j in range(self.PointNumber):
                gradient[i] = gradient[i] + (dw[j]*self.Model[j])
        return gradient

    def EvaluateVarianceHessian(self,p):
        Variance=np.zeros((self.PointNumber))
        for j in range(self.PointNumber):
            Dist=smc.L2Norm(self.Points[j],p)
            Variance[j]=self.V[0]- Vario(Dist, self.V[0], self.V[1])

        e,w = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
        hessian = np.zeros((self.dim,self.dim))
        for i in range(self.dim): #loop through dimensions
            A1 = dw_dp(p,i, self.PointNumber, self.C_Inv, self.Points, self.V)
            B1 = dD_dp(p,i, self.PointNumber, self.Points, self.V)
            for j in range(self.dim): #loop through dimensions
                A2 = dw_dp(p,j, self.PointNumber, self.C_Inv, self.Points, self.V)
                B2 = dD_dp(p,j, self.PointNumber, self.Points, self.V)
                C = d2w_dp2(p,i,j, self.PointNumber, self.C_Inv, self.Points, self.V)
                D = d2D_dp2(p,i,j, self.PointNumber, self.Points, self.V)
                for k in range(self.PointNumber):
                    hessian[i,j] = hessian[i,j] + \
                    ((C[k]*Variance[k])+(A1[k]*B2[k])+(A2[k]*B1[k])+(w[k]*D[k]))
                hessian[i,j] = -hessian[i][j] - d2l_dp2(p,i,j, self.PointNumber, self.C_Inv, self.Points, self.V)
        return hessian

    def EvaluateMeanHessian(self,p):
        hessian = np.zeros((self.dim,self.dim))
        for i in range(self.dim):
            for j in range(self.dim):
                dw = d2w_dp2(p,i,j, self.PointNumber, self.C_Inv, self.Points, self.V)
                for k in range(self.PointNumber):
                    hessian[i][j] = hessian[i][j] + (dw[k]*self.Model[k])
        return hessian

###########################################################################
###########################################################################
###########################################################################

