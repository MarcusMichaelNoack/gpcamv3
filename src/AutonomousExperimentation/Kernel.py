import Misc as smc
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
import math
import Config as conf
import numba_wrapper as nb
from scipy import *
from Optimization import *
from Misc import *
from scipy.optimize import differential_evolution
from scipy.sparse.linalg import cgs
from scipy.sparse.linalg import minres
import time
class Kernel:
    def __init__(self,points,model,variance,model_name, bounds,ReadHyperParameters = False):
        self.Points = points ###location of points as array
        self.PointNumber = len(self.Points)
        self.Model  = model   ###model function evaluations as 1d array
        self.model_name = model_name
        self.bounds = bounds
        self.Variances = variance   ###variances as 1d array
        self.dim = len(self.Points[0])
        self.v = self.FindMaternParameters()
        HyperParameters = np.ones((self.dim+1)) * 2.0
        self.Mean = np.zeros((len(self.Model))) + np.mean(self.Model)
        self.ExpertMode = conf.ModelFunctions[model_name]['expert knowledge']['switch']
        self.HyperParameterOptimizationBounds = self.ExtractOptimizationBounds()
        if ReadHyperParameters == False:
            self.K,self.K_Inv,self.HyperParameters = self.FindHyperParameters(HyperParameters, True)
            np.save("./data/current_data/HyperParameters",self.HyperParameters)
        else:
            self.HyperParameters = np.load("./data/current_data/HyperParameters.npy")
            self.K  = self.ComputeCovariance(self.HyperParameters)
            self.K_Inv = np.linalg.inv(self.K)
        #self.K,Prod,self.HyperParameters = self.FindHyperParameters(HyperParameters, True)

    def UpdateKernel(self,points,model,variance, mode):
        self.Points = points
        self.PointNumber = len(self.Points)
        self.Model  = model
        self.Mean = np.zeros((len(self.Model))) + np.mean(self.Model)
        self.Variances = variance
        self.NumberPoints = len(self.Points)
        if mode == 'light':
            if conf.Rank_n_Update == True: self.K, self.K_Inv =  self.CovarianceUpdate()
            else: self.K  = self.ComputeCovariance(self.HyperParameters); self.K_Inv = np.linalg.inv(self.K)
        if mode == 'full' : self.K,self.K_Inv,self.HyperParameters = self.FindHyperParameters(self.HyperParameters,False)

    def ComputeCovariance(self,HyperParameters):
        #st = time.time()
        CoVariance = self.KernelMaternNDVec(self.Points,HyperParameters)
        #et = time.time()
        #print("time to compute covariance: ", et-st)
        smc.AddToDiag(CoVariance,self.Variances)
        #CoVarianceInverse = np.linalg.inv(CoVariance)
        return CoVariance

    def CovarianceUpdate(self):
        #st = time.time()
        CoVariance = self.KernelMaternNDVec(self.Points,self.HyperParameters)
        smc.AddToDiag(CoVariance,self.Variances)
        OldInverse = np.array(self.K_Inv)
        r = len(OldInverse)
        c = len(OldInverse[0])
        F = CoVariance[0:r,c: ]
        G = CoVariance[r: ,0:c]
        H = CoVariance[r: ,c: ]
        EI = OldInverse

        S = H - (G.dot(EI).dot(F))
        SI = np.linalg.inv(S)
        CoVarianceInverse = np.block([[ EI + EI.dot(F).dot(SI).dot(G).dot(EI), -EI.dot(F).dot(SI)],
                                    [-SI.dot(G).dot(EI)                    ,  SI               ]])
        #et = time.time()
        #print("time to update cov: ", et-st)
        return CoVariance,CoVarianceInverse


    def FindMaternParameters(self):
        v = np.zeros((self.dim))
        index = 0
        if conf.TimeSeries[0] == True:
            v[:] = 1.5
            return v
        for name in list(conf.Parameters):
            if conf.ModelFunctions[self.model_name]['expert knowledge']['differentiability'][name] == 0: v[index] = 0.5
            if conf.ModelFunctions[self.model_name]['expert knowledge']['differentiability'][name] == 1: v[index] = 1.5
            if conf.ModelFunctions[self.model_name]['expert knowledge']['differentiability'][name] == 2: v[index] = 2.5
            if conf.ModelFunctions[self.model_name]['expert knowledge']['differentiability'][name] == 'inf': v[index] = 10e6
            if conf.ModelFunctions[self.model_name]['expert knowledge']['differentiability'][name] == 'sparse': v[index] = 20e6
            index = index + 1
        return v

    def ExtractOptimizationBounds(self):
        bounds = np.zeros((self.dim+1,2))
        if conf.TimeSeries[0] == False:
            for i in range(len(conf.OptimizationBounds)):
                bounds[i,0] = conf.OptimizationBounds[i][0]
                bounds[i,1] = conf.OptimizationBounds[i][1]
        else:
            bounds[0,0] = conf.OptimizationBounds[0][0]
            bounds[0,1] = conf.OptimizationBounds[0][1]
            for i in range(1,len(bounds)):
                bounds[i,0] = conf.OptimizationBounds[1][0]
                bounds[i,1] = conf.OptimizationBounds[1][1]
        return bounds

    def FindHyperParameters(self,HPs,FromScratch):
        ####here, one could find hyper parameters by using an expert model, but this 
        ####is not what we are doing so far
        #if self.ExpertMode == 'on':
        #    K,K_Inv = self.MinimizeKLDivergency()  ### not clear how exactly to do this the optimal way
        #else:
        K,HyperParameters = self.MaxLogLikelihood(HPs,FromScratch)
        K_Inv = np.linalg.inv(K)
        return K,K_Inv,HyperParameters

    def MaxLogLikelihood(self,HPs, FromScratch = False):
        epsilon = np.inf
        step_size = 1.0
        print("Hyper-parameter tuning in progress. Old hyper-parameters: ",HPs,\
                " with old log likelihood: ",self.LogLikelihood(HPs))
        if FromScratch == True or np.random.rand() > conf.HyperParameterUpdateRecomputationRatio:
            print("I am performing a genetic algorithm to find the optimal hyper-parameters.")
            res  = differential_evolution(self.LogLikelihoodS,self.HyperParameterOptimizationBounds, disp = True, maxiter = 120)
            HyperParameters1 = np.array(res['x'])
            Eval1 = self.LogLikelihood(HyperParameters1)
            HyperParameters = np.array(HyperParameters1)
            print("I found the hyper-parameters ",HyperParameters," with likelihood ",self.LogLikelihood(HyperParameters))
        else:
            HyperParameters = np.array(HPs)
            print("This is not the first time I am looking for hyper-parameters, so I'll start with what I have and do an update...")

        evaluation = self.LogLikelihood(HyperParameters)
        #OptimumPosition, OptimumEvaluation, success = \
        #Newton(HyperParameters,self.HyperParameterOptimizationBounds,
        #self.LogLikelihood,self.LogLikelihoodGradientH,\
        #self.LogLikelihoodHessianH, start = 'x0')
        #OptimumPosition, OptimumEvaluation, success = \
        #GradientDescent(HyperParameters,self.HyperParameterOptimizationBounds,
        #self.LogLikelihood,self.NumGrad, 1)
        print("I am starting a gradient-based optimization...")
        OptimumPosition, OptimumEvaluation, success = \
        GradientDescent(HyperParameters,self.HyperParameterOptimizationBounds,
        self.LogLikelihood,self.LogLikelihoodGradientH, 1)
        print("Gradient-based optimization concluded with result: ",OptimumPosition)

        if OptimumEvaluation > evaluation:
            HyperParameters = OptimumPosition

        print("Hyper-Parameter tuning successfully concluded, new hyper-parameters: ",\
        HyperParameters, "with log likelihood: ",self.LogLikelihood(HyperParameters))
        K  = self.ComputeCovariance(HyperParameters)
        return K,HyperParameters

    def LogLikelihood(self,HyperParameters):
        K = self.ComputeCovariance(HyperParameters)
        K_Inv = np.linalg.inv(K)
        y = self.Model
        sign,logdet = np.linalg.slogdet(K)
        return [(-0.5 * ((y-self.Mean).dot(K_Inv).dot(y-self.Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]
        #if SystemSolver == 'INV':
        #    K_Inv = np.linalg.inv(K)
        #    return [(-0.5 * ((y-Mean).dot(K_Inv).dot(y-Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]
        #elif SystemSolver == 'CG':
        #    b,success = cgs(K,y-Mean,maxiter = 20000,M = np.diag(1.0/np.diag(K)))
        #    if success == 0: 
        #        return [(-0.5 * ((y-Mean).dot(b))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]
        #    else:
        #        K_Inv = np.linalg.inv(K)
        #        return [(-0.5 * ((y-Mean).dot(K_Inv).dot(y-Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]
        #elif SystemSolver == 'MINRES':
        #    b,success = minres(K, y-Mean,maxiter = 20000)
        #    if success == 0: 
        #        return [(-0.5 * ((y-Mean).dot(b))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]
        #    else: 
        #        K_Inv = np.linalg.inv(K)
        #        return [(-0.5 * ((y-Mean).dot(K_Inv).dot(y-Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi))]

    def LogLikelihoodS(self,HyperParameters):
        K = self.ComputeCovariance(HyperParameters)
        K_Inv = np.linalg.inv(K)
        y = self.Model
        sign,logdet = np.linalg.slogdet(K)
        return -((-0.5 * ((y-self.Mean).dot(K_Inv).dot(y-self.Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi)))

    def LogLikelihoodGradientH(self,HyperParameters):
        K =  self.ComputeCovariance(HyperParameters)
        K_Inv = np.linalg.inv(K)
        y = self.Model
        dL_dH = np.zeros((self.dim+1))
        dK_dH = self.Compute_dK_dH(HyperParameters)
        NumGrad = np.zeros((self.dim+1))

        for i in range(self.dim+1):
            dL_dH[i] = 0.5 * ((((y-self.Mean).T).dot(K_Inv).dot(dK_dH[:,:,i]).\
            dot(K_Inv).dot(y-self.Mean))-(np.trace(np.matmul(K_Inv,dK_dH[:,:,i]))))
        return dL_dH

    #def LogLikelihoodGradientM(self,HyperParameters,K_Inv):
    #    y = self.Model
    #    B = K_Inv+K_Inv.T
    #    NumGrad = np.zeros((self.dim+1))
    #    ee = 0.0001
    #    for i in range(self.dim+1):
    #
    #        #self.HyperParameters[i] = self.HyperParameters[i] - ee
    #        #A = self.LogLikelihood(self.HyperParameters)
    #        #self.HyperParameters[i] = self.HyperParameters[i] + (2.0*ee)
    #        #B = self.LogLikelihood(self.HyperParameters)
    #        #self.HyperParameters[i] = self.HyperParameters[i] - ee
    #        #NumGrad[i] = (B - A)/(2.0*ee)
#
#        #prdint("Numerical  Gradient: ", NumGrad)
#        #prdint("Analytical Gradient: ", dL_dH)
#        retdurn dL_dm

    def LogLikelihoodHessianH(self,HyperParameters):
        K, K_Inv = ComputeCovariance(self.Points,self.PointNumber,self.Variances,
        self.KernelMaternNDVec,HyperParameters)

        y = self.Model - self.Mean
        d2L_dH2 = np.zeros((self.dim+1,self.dim+1))
        dK_dH =   self.Compute_dK_dH(HyperParameters)
        d2K_dH2 = self.Compute_d2K_dH2(HyperParameters)
        NumGrad = np.zeros((self.dim+1))
        for i in range(self.dim+1):
            for j in range(self.dim+1):
                A = (-K_Inv.dot(dK_dH[:,:,j]).dot(K_Inv).dot(dK_dH[:,:,i]).dot(K_Inv)) -\
                    (K_Inv.dot(dK_dH[:,:,i]).dot(K_Inv).dot(dK_dH[:,:,j]).dot(K_Inv))  +\
                    (K_Inv.dot(d2K_dH2[:,:,i,j]).dot(K_Inv))
                B = np.trace(((-K_Inv.dot(dK_dH[:,:,j]).dot(K_Inv)).dot(dK_dH[:,:,i]))+(K_Inv.dot(d2K_dH2[:,:,i,j])))
                d2L_dH2[i,j] = 0.5 * (((y).T).dot(A).dot(y) - B)

        return d2L_dH2
    ##################################################
    #########Kernels##################################
    ##################################################

    def KernelSquaredExp(self, distance, length):
        kernel = np.exp(-(distance**2)/(2.0*(length**2)))
        return kernel

    def KernelExp(self, distance, length):
        kernel = np.exp(-(distance)/(length))
        return kernel

    def KernelMaternP1(self, distance, length):
        kernel = (1.0+((np.sqrt(3.0)*distance)/(length))) * np.exp(-(np.sqrt(3.0)*distance)/length)
        return kernel

    def KernelMaternP2(self, distance, length):
        kernel = (1.0+((np.sqrt(5.0)*distance)/(length))+((5.0*distance**2)/(3.0*length**2))) * np.exp(-(np.sqrt(5.0)*distance)/length)
        return kernel

    def BumpFunctionKernel(self,distance,beta,r):
        if distance > r: kernel = 0.0
        else: kernel = np.exp(-beta/(1.0-(distance/r)**2))/np.exp(-beta)
        return kernel

    def KernelMatern(self,distance_matrix,v, length):
        if v == 0.5: return self.KernelExp(distance_matrix,length)
        elif v == 1.5: return self.KernelMaternP1(distance_matrix,length)
        elif v == 2.5: return self.KernelMaternP2(distance_matrix,length)
        elif v == 10e6: return self.KernelSquaredExp(distance_matrix,length)
        elif v == 20e6: return self.BumpFunctionKernel(distance_matrix,length)
        else: print("no differentiability parameter given in Config.py for matern.");print(v);exit()

    ##################################################
    #########Derivatives with respect to p ###########
    ##################################################

    def d_KernelSquaredExp_dp(self, distance, length):
        dkernel_dp = -(distance/length**2)*np.exp(-(distance**2)/(2.0*(length**2)))
        return dkernel_dp

    def d_KernelExp_dp(self, distance, length):
        dkernel_dp = -(1.0/length)*np.exp(-(distance)/(length))
        return dkernel_dp

    def d_KernelMaternP1_dp(self, distance, length):
        ee = np.exp(-(np.sqrt(3.0)*distance)/length)
        a  = np.sqrt(3.0)/length
        b  = a*distance
        dkernel_dp = a*(ee-(ee*(1.0+b)))
        return dkernel_dp

    def d_KernelMaternP2_dp(self, distance, length):
        ee = np.exp(-(np.sqrt(5.0)*distance)/length)
        a  = np.sqrt(5.0)/length
        b  = a * distance
        c = (10.0*distance)/(3.0*length**2)
        d = (5.0 *distance**2)/(3.0*length**2)
        dkernel_dp = ((a+c)*ee)-((1.0+b+d)*a*ee)
        return dkernel_dp

    def d_BumpFunctionKernel_dp(self,distance,beta,r):
        print("using the bump function is in an experimental state, so are the derivatives of the bump function, proceed with caution.")
        if distance > r: kernel = 0.0
        else: dkernel_dp = ((-2.0*beta*(distance/r)**2*np.exp(-beta/(1.0-(distance/r)**2)))/((1.0-(distance/r)**2)**2))/np.exp(-beta)
        return dkernel_dp

    def d_KernelMatern_dp(self,distance_matrix,v, length):
        if v == 0.5:    return self.d_KernelExp_dp(distance_matrix,length)
        elif v == 1.5:  return self.d_KernelMaternP1_dp(distance_matrix,length)
        elif v == 2.5:  return self.d_KernelMaternP2_dp(distance_matrix,length)
        elif v == 10e6: return self.d_KernelSquaredExp_dp(distance_matrix,length)
        elif v == 20e6: return self.d_BumpFunctionKernel_dp(distance_matrix,length)
        else: print("no differentiability parameter given in Config.py for matern.");print(v);exit()


    ##################################################
    #########Derivatives with respect to length scale#
    ##################################################
    def KernelSquaredExpDer(self,distance,length):
        #returns the derivative of the 1d squared exponential kernel
        #with respect to length scale
        return self.KernelSquaredExp(distance,length)*((distance**2)/(length**3))

    def KernelExpDer(self,distance,length):
        #returns the derivative of the 1d exponential kernel
        #with respect to length scale
        return np.exp(-(distance)/(length)) * (distance/(length**2))

    def KernelMaternP1Der(self,distance,length):
        #returns the derivative of the 1d Matern kernel (P1)
        #with respect to length scale
        return np.exp(-((np.sqrt(3.0)*distance)/(length)))*\
               ((3.0*distance**2)/(length**3))

    def KernelMaternP2Der(self,distance,length):
        #returns the derivative of the 1d Matern kernel (P1)
        #with respect to length scale
        a = np.exp(-(np.sqrt(5.0)*distance)/(length))
        return a*((distance**2*((5.0*length)+(5.0**(3.0/2.0)*distance)))/(3.0*length**4))

    def BumpFunctionKernelDer(self,distance,beta,r):
        if distance < r: return ((-2.0*beta*r**2*distance*self.BumpFunctionKernel(distance,beta,r))/(x**2-r**2)**2)/np.exp(-beta)
        else: return 0.0

    def KernelSquaredExpDer2(self,distance,length):
        #returns the derivative of the 1d squared exponential kernel
        #with respect to length scale
        return (self.KernelSquaredExp(distance,length)*((distance**4)/(length**6)))-\
               (3.0*selfKernel.SquaredExpDer(distance,length)*((distance**2)/(length**4)))

    def KernelExpDer2(self,distance,length):
        #returns the derivative of the 1d exponential kernel
        #with respect to length scale
        ee = self.KernelExp(distance,length)
        return ee*((-2.0*distance/(length**3))+((distance**2)/(length**4)))

    def KernelMaternP1Der2(self,distance,length):
        #returns the derivative of the 1d Matern kernel (P1)
        #with respect to length scale
        ee = np.exp((-np.sqrt(3.0)*distance)/(length))
        return ee*(((3.0**(3.0/2.0)*distance**3)/(length**5))-((9.0*distance**2)/(length**4)))

    def KernelMaternP2Der2(self,distance,length):
        #returns the derivative of the 1d Matern kernel (P1)
        #with respect to length scale
        a = np.exp(-(np.sqrt(5.0)*distance)/(length))
        return a*(((15.0*length**2)+(3.0*5.0**(3.0/2.0)*distance*length)-(25.0*distance**2))/(3.0*length**6))

    def BumpFunctionKernelDer2(self,distance,beta,r):
        A = distance**2 - r**2
        B = BumbFunction(distance,beta,r)
        if distance < r: return ((2*beta*r**2)*(3.0*distance**4+((2.0*beta-2)*r**2*distance**2)-r**4)*B)/A**4
        else: return 0.0

    def KernelMaternGradient(self,distance_matrix,v,length):
        if v == 0.5:    grad = self.KernelExpDer(distance_matrix,length)
        elif v == 1.5:  grad = self.KernelMaternP1Der(distance_matrix,length)
        elif v == 2.5:  grad = self.KernelMaternP2Der(distance_matrix,length)
        elif v == 10e6: grad = self.KernelSquaredExpDer(distance_matrix,length)
        elif v == 20e6: grad = self.BumpFunctionKernelDer(distance_matrix,beta,length)
        else: print("An error has occurred with the  differentiability parameter given in Config.py for matern.");print(v);exit()
        return grad

    def KernelMaternHessian(self,distance_matrix,v,length):
        if v == 0.5:    h =  self.KernelExpDer2(distance_matrix,length)
        elif v == 1.5:  h =  self.KernelMaternP1Der2(distance_matrix,length)
        elif v == 2.5:  h =  self.KernelMaternP2Der2(distance_matrix,length)
        elif v == 10e6: h =  self.KernelSquaredExpDer2(distance_matrix,length)
        elif v == 20e6: grad = self.BumpFunctionKernelDer2(distance_matrix,length)
        else: print("An error has occurred with the  differentiability parameter given in Config.py for matern.");print(v);exit()
        return h

############################################################
############################################################
############################################################

    def KernelMaternND(self,p1,p2,HyperParameters):
        kernel = 1.0
        for i in range(self.dim):
            kernel = kernel * self.KernelMatern(abs(p1[i]-p2[i]),self.v[i],HyperParameters[i+1])
        return HyperParameters[0] * kernel

    def KernelMaternNDVec(self,points,HyperParameters):
        kernel = 1.0
        for i in range(self.dim):
            distance_matrix = np.abs(np.subtract.outer(points[:,i],points[:,i]))
            kernel = kernel * self.KernelMatern(distance_matrix,self.v[i],HyperParameters[i+1])
        return HyperParameters[0] * kernel

    def d_KernelMaternND_dp(self,p1,p2,HyperParameters,direction):
        kernel = 1.0
        for i in range(self.dim):
            if i == direction: continue
            kernel = kernel * self.KernelMatern(abs(p1[i]-p2[i]),self.v[i],HyperParameters[i+1])
        return HyperParameters[0] * kernel * \
               self.d_KernelMatern_dp(abs(p1[direction]-p2[direction]),\
               self.v[direction],HyperParameters[direction+1])*-np.sign(p1[direction]-p2[direction])

    def d_KernelMaternND_dpVec(self,points,HyperParameters,direction):
        kernel = 1.0
        for i in range(self.dim):
            if i == direction: continue
            distance_matrix = np.abs(np.subtract.outer(points[:,i],points[:,i]))
            kernel = kernel * self.KernelMatern(distance_matrix,self.v[i],HyperParameters[i+1])
        distance_matrix = np.abs(np.subtract.outer(points[:,direction],points[:,direction]))
        return HyperParameters[0] * kernel * self.d_KernelMatern_dp(distance_matrix,\
               self.v[direction],HyperParameters[direction+1])

    def KernelMaternNDGradient(self,p1,p2,HyperParameters):
        kernel_grad = np.ones((len(HyperParameters)))
        for i in range(self.dim):
            for j in range(self.dim):
                if i == j: continue
                kernel_grad[i+1] = kernel_grad[i+1] * self.KernelMatern(abs(p1[j]-p2[j]),self.v[j],HyperParameters[j+1])
            kernel_grad[i+1] = kernel_grad[i+1] * self.KernelMaternGradient(abs(p1[i]-p2[i]),self.v[i],HyperParameters[i+1])*HyperParameters[0]
        kernel_grad[0] = self.KernelMaternND(p1,p2,HyperParameters)/HyperParameters[0]
        return kernel_grad

    def KernelMaternNDGradientVec(self,points,HyperParameters):
        kernel_grad = np.ones((len(HyperParameters),len(points),len(points)))
        for i in range(self.dim):
            for j in range(self.dim):
                if i == j: continue
                distance_matrix = np.abs(np.subtract.outer(points[:,j],points[:,j]))
                kernel_grad[i+1] = kernel_grad[i+1] * self.KernelMatern(distance_matrix,self.v[j],HyperParameters[j+1])
            distance_matrix = np.abs(np.subtract.outer(points[:,i],points[:,i]))
            kernel_grad[i+1] = kernel_grad[i+1] * \
            self.KernelMaternGradient(distance_matrix,self.v[i],HyperParameters[i+1])*HyperParameters[0]
        kernel_grad[0] = self.KernelMaternNDVec(points,HyperParameters)/HyperParameters[0]
        return kernel_grad

    def KernelMaternNDHessian(self,p1,p2,HyperParameters):
        kernel_hessian = np.ones((len(HyperParameters),len(HyperParameters)))
        A = np.ones((len(HyperParameters),len(HyperParameters)))
        B = np.ones((len(HyperParameters),len(HyperParameters)))
        for i in range(self.dim):
            for j in range(self.dim):
                for k in range(self.dim):
                    if k == i: continue
                    A[i+1,j+1] = A[i+1,j+1] * self.KernelMatern(p1[k],p2[k],self.v[k],HyperParameters[k+1])
                    if k == i or k == j: continue
                    B[i+1,j+1] = A[i+1,j+1] * self.KernelMatern(p1[k],p2[k],self.v[k],HyperParameters[k+1])
                kernel_hessian[i+1,j+1] = ((B[i+1,j+1] * \
                self.KernelMaternGradient(p1[i],p2[i],self.v[i],HyperParameters[i+1])*\
                self.KernelMaternGradient(p1[j],p2[j],self.v[j],HyperParameters[j+1]))+(\
                KroneckerDelta(i,j)*A[i+1,j+1]*self.KernelMaternHessian(p1[i],p2[i],self.v[i],HyperParameters[i+1])))*\
                HyperParameters[0]
        kernel_hessian[0,:] = self.KernelMaternNDGradient(p1,p2,HyperParameters)/HyperParameters[0]
        kernel_hessian[:,0] = kernel_hessian[0,:]
        kernel_hessian[0,0] = 0.0
        return kernel_hessian

    def KernelMaternNDHessianVec(self,points,HyperParameters):
        kernel_hessian = np.ones((len(HyperParameters),len(HyperParameters),len(points),len(points)))
        A = np.ones((len(HyperParameters),len(HyperParameters),len(points),len(points)))
        B = np.ones((len(HyperParameters),len(HyperParameters),len(points),len(points)))
        for i in range(self.dim):
            for j in range(self.dim):
                for k in range(self.dim):
                    if k == i: continue
                    distance_matrix = np.abs(np.subtract.outer(points[:,k],points[:,k]))
                    A[i+1,j+1] = A[i+1,j+1] * self.KernelMatern(distance_matrix,self.v[k],HyperParameters[k+1])
                    if k == i or k == j: continue
                    B[i+1,j+1] = A[i+1,j+1] * self.KernelMatern(distance_matrix,self.v[k],HyperParameters[k+1])
                distance_matrix_i = np.abs(np.subtract.outer(points[:,i],points[:,i]))
                distance_matrix_j = np.abs(np.subtract.outer(points[:,j],points[:,j]))
                kernel_hessian[i+1,j+1] = ((B[i+1,j+1] * \
                self.KernelMaternGradient(distance_matrix_i,self.v[i],HyperParameters[i+1])*\
                self.KernelMaternGradient(distance_matrix_j,self.v[j],HyperParameters[j+1]))+(\
                KroneckerDelta(i,j)*A[i+1,j+1]*self.KernelMaternHessian(distance_matrix_i,self.v[i],HyperParameters[i+1])))*\
                HyperParameters[0]
        kernel_hessian[0,:] = self.KernelMaternNDGradientVec(points,HyperParameters)/HyperParameters[0]
        kernel_hessian[:,0] = kernel_hessian[0,:]
        kernel_hessian[0,0] = 0.0
        return kernel_hessian

    def Compute_dK_dH(self,HyperParameters):
        dK_dH = np.zeros((self.PointNumber,self.PointNumber,self.dim+1))
        for i in range(self.PointNumber):
            for j in range(self.PointNumber):
                dK_dH[i,j] = self.KernelMaternNDGradient(self.Points[i],self.Points[j],HyperParameters)
        #########TEST####################
        #e = 0.0001
        #A,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec)
        #self.HyperParameters[2] = self.HyperParameters[2] + e
        #B,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec)
        #self.HyperParameters[2] = self.HyperParameters[2] - e
        #print("Num Grad: ",(B-A)/e)
        #print("Ana Grad: ",dK_dH[:,:,2])
        #print("Differen: ",dK_dH[:,:,2]-((B-A)/e))
        #########TEST####################
        return dK_dH

    def Compute_d2K_dH2(self,HyperParameters):
        d2K_dH2 = np.zeros((self.PointNumber,self.PointNumber,self.dim+1,self.dim+1))
        for i in range(self.PointNumber):
            for j in range(self.PointNumber):
                d2K_dH2[i,j] = self.KernelMaternNDHessian(self.Points[i],self.Points[j],HyperParameters)
        """
        #########TEST####################
        e = 0.0001
        index1 = 2
        index2 = 0
        A,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec,HyperParameters)

        HyperParameters[index1] = HyperParameters[index1] + e
        HyperParameters[index2] = HyperParameters[index2] + e
        A_pp,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec,HyperParameters)
        HyperParameters[index1] = HyperParameters[index1] - e
        HyperParameters[index2] = HyperParameters[index2] - e

        HyperParameters[index1] = HyperParameters[index1] + e
        HyperParameters[index2] = HyperParameters[index2] - e
        A_pm,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec,HyperParameters)
        HyperParameters[index1] = HyperParameters[index1] - e
        HyperParameters[index2] = HyperParameters[index2] + e

        HyperParameters[index1] = HyperParameters[index1] - e
        HyperParameters[index2] = HyperParameters[index2] + e
        A_mp,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec,HyperParameters)
        HyperParameters[index1] = HyperParameters[index1] + e
        HyperParameters[index2] = HyperParameters[index2] - e

        HyperParameters[index1] = HyperParameters[index1] - e
        HyperParameters[index2] = HyperParameters[index2] - e
        A_mm,aux = ComputeCovariance(self.Points,self.PointNumber,self.Variances,self.KernelMaternNDVec,HyperParameters)
        HyperParameters[index1] = HyperParameters[index1] - e
        HyperParameters[index2] = HyperParameters[index2] - e



        print("Num Hessian: ",((A_pp)-(A_pm)-(A_mp)+(A_mm))/(4.0*e**2))
        print("Ana Hessian: ",d2K_dH2[:,:,index1,index2])
        #########TEST####################

        #exit()
        """
        return d2K_dH2





    def NumGrad(self,HyperParameters):
        #########FD Test of derivatives:
        gr = np.zeros((3))
        h = np.zeros((3,3))
        for index in range(3):
            B1 = self.LogLikelihood(HyperParameters)
            HyperParameters[index] = HyperParameters[index] + 0.00001
            B2 = self.LogLikelihood(HyperParameters)
            HyperParameters[index] = HyperParameters[index] - 0.00001
            gr[index] = (B2[0]-B1[0])/0.00001
        #print("num grad: ",gr," at ",HyperParameters)
        #print(gr)
        return gr


    def NumHessian(self,HyperParameters):
        #########FD Test of derivatives:
        h = np.zeros((3,3))
        e = 0.0001
        for index1 in range(0,3):
            for index2 in range(0,3):
                HyperParameters[index1] = HyperParameters[index1] + e
                HyperParameters[index2] = HyperParameters[index2] + e
                A_pp = self.LogLikelihood(HyperParameters)
                HyperParameters[index1] = HyperParameters[index1] - e
                HyperParameters[index2] = HyperParameters[index2] - e

                HyperParameters[index1] = HyperParameters[index1] + e
                HyperParameters[index2] = HyperParameters[index2] - e
                A_pm = self.LogLikelihood(HyperParameters)
                HyperParameters[index1] = HyperParameters[index1] - e
                HyperParameters[index2] = HyperParameters[index2] + e

                HyperParameters[index1] = HyperParameters[index1] - e
                HyperParameters[index2] = HyperParameters[index2] + e
                A_mp = self.LogLikelihood(HyperParameters)
                HyperParameters[index1] = HyperParameters[index1] + e
                HyperParameters[index2] = HyperParameters[index2] - e

                HyperParameters[index1] = HyperParameters[index1] - e
                HyperParameters[index2] = HyperParameters[index2] - e
                A_mm = self.LogLikelihood(HyperParameters)
                HyperParameters[index1] = HyperParameters[index1] - e
                HyperParameters[index2] = HyperParameters[index2] - e

                h[index1,index2] = ((A_pp[0])-(A_pm[0])-(A_mp[0])+(A_mm[0]))\
                /(4.0*e**2)

        print("Num Hessian: ")
        print(h)
        return h

    def Plot_L(self):
        from matplotlib import pyplot as plt
        from matplotlib import cm
        from mpl_toolkits.mplot3d import Axes3D
        x1 = np.linspace(5,25.0,30)
        x2 = np.linspace(5,25.0,30)
        X,Y= np.meshgrid(x1,x2)
        Z1 = np.zeros((X.shape))
        Z2 = np.zeros((X.shape))
        Z3 = np.zeros((X.shape))
        for i in range(len(x1)):
            for j in range(len(x2)):
                Z1[i,j] = self.LogLikelihood([0.0457,X[i,j],Y[i,j]])[0]
        fig1 = plt.figure(1)
        ax1 = Axes3D(fig1)
        ax1.plot_surface(X,Y,Z1,rstride = 1, cstride = 1,cmap = cm.viridis)
        ax1.set_xlabel("lx")
        ax1.set_ylabel("ly")
        plt.show()
        exit()

