########################
##Expert Model Class####
########################

import random
import numpy as np
import Config as conf
import Misc as smc
from Kernel import *
from Kernel import Kernel as kernel
import ExperimentControls as exp
import itertools
import matplotlib.pyplot as plt
from MeasurementCosts import Cost as costs
import Optimization as opt
from scipy.interpolate import griddata
import time
from Kernel import Kernel as kernel


class DomainExpertGP:
    def __init__(self,domain_expert_data,model_name, experiment_data):
        
        self.model_name = model_name
        self.ExpertGaussianProcesss = conf.ModelFunctions[model_name]['expert knowledge']['expert models']
        self.bounds = conf.ModelFunctions[model_name]['expert knowledge']['bounds']
        self.parameter_bounds = experiment_data.bounds
        self.dim = experiment_data.dim
        self.experiment_data = experiment_data
        self.domain_expert_data = domain_expert_data
        #self.Points = self.experiment_data.Points
        #self.PointNumber = len(self.Points)
        #self.ExpertFunctions = self.LoadParametricExpertFunctions()
        #self.ModelFunctionEvals = ModelFunctionEvals
        #self.Coefficients = self.OptimizeCoefficients()
        #print("Coefficients Updated")
        self.ExpertGP = {}
        self.Gaussians= {}
        for expert_model in conf.ModelFunctions[model_name]['expert knowledge']['expert models']:
            if conf.ModelFunctions[model_name]['expert knowledge']['expert models'][expert_model]['source'] == 'points':
                self.ExpertGP[expert_model] = self.CreatePointGP(model_name,expert_model)
                self.Gaussians[expert_model] = \
                [self.ExpertGP[expert_model].Mean,self.ExpertGP[expert_model].K,self.ExpertGP[expert_model].K_Inv]
            if conf.ModelFunctions[model_name]['expert knowledge']['expert models'][expert_model]['source']  == 'parametric':
                self.ExpertGP[expert_model]  = self.CreateParametricGP(model_name,expert_model)
                self.Gaussians[expert_model] = \
                [self.ExpertGP[expert_model].Mean,self.ExpertGP[expert_model].K,self.ExpertGP[expert_model].K_Inv]
            if conf.ModelFunctions[model_name]['expert knowledge']['expert models'][expert_model]['source']  == 'simulation':
                self.ExpertGP[expert_model]  = self.CreatePointGP(model_name,expert_model)
                self.Gaussians[expert_model] = \
                [self.ExpertGP[expert_model].Mean,self.ExpertGP[expert_model].K,self.ExpertGP[expert_model].K_Inv]
        #print(self.EvaluatePointExpertGP(np.array([0.1,5.0]),'expert_model_1'))

    def UpdateExpertGP(self,domain_expert_data,experiment_data):
        self.experiment_data = experiment_data
        self.domain_expert_data = domain_expert_data
        return 0

    def CreatePointGP(self,model_name,expert_model):
        print("    Looking for correlations in the domain expert point data...")
        points =    self.domain_expert_data.ExpertPointData[expert_model][:,0:self.dim]
        model =     self.domain_expert_data.ExpertPointData[expert_model][:,self.dim]
        variances = self.domain_expert_data.ExpertPointData[expert_model][:,self.dim+1]
        ExpertPointsGP = kernel(points,model,variances,model_name,self.parameter_bounds)
        return ExpertPointsGP


    def EvaluateExpertGP(self,name,p):
        #k  = np.zeros((self.PointNumber))
        #fv  = np.zeros((self.PointNumber))
        #mean = self.Gaussians[name][0]
        #K = self.Gaussians[name][1]
        #K_Inv = self.Gaussians[name][2]

        if self.ExpertGaussianProcesss[name]['source'] == 'parametric':
            for i in range(self.PointNumber):
                    fv[i],k[i] = self.EvaluateParametricExpertGaussianProcess(p,self.Points[i],name,self.Coefficients[name])
            return (k.dot(K_Inv)).dot(fv-mean)
        elif self.ExpertGaussianProcesss[name]['source'] == 'points':
            return self.EvaluatePointExpertGP(p,name)
        else:
            print("no expert source");exit()

    def EvaluateExpertErrorFunction(self,name,p):
        k  = np.zeros((self.PointNumber))
        fv  = np.zeros((self.PointNumber))
        mean = self.Gaussians[name][0]
        K = self.Gaussians[name][1]
        K_Inv = self.Gaussians[name][2]

        if self.ExpertGaussianProcesss[name]['source'] == 'parametric':
            for i in range(self.PointNumber):
                fv[i],k[i] = self.EvaluateParametricExpertGaussianProcess(p,self.Points[i],name,self.Coefficients[name])
            if self.EvaluateParametricExpertGaussianProcess(p,p,name,self.Coefficients[name])[1] - (k.dot(K_Inv)).dot(k) == 0:
                print("0 in parametric"); exit()
            return self.EvaluateParametricExpertGaussianProcess(p,p,name,self.Coefficients[name])[1] - (k.dot(K_Inv)).dot(k)
        elif self.ExpertGaussianProcesss[name]['source'] == 'points':
            if self.EvaluatePointExpertError(p,name) == 0:
                print("0 in points");exit()
            return self.EvaluatePointExpertError(p,name)
        else:
            print("no expert source");exit()

    def OptimizeCoefficients(self):
        ##max likelihood to find best coefficients for each parametric model
        Coefficients = {}
        print("Optimizing coefficients for expert models for: ", self.model_name)
        """
        #######################################
        ###Just A Plotting Test Here
        #######################################
        CoVarianceMatrix = np.zeros((len(self.Points),len(self.Points)))
        Mean = np.zeros((len(self.Points)))
        bounds = self.data.ParametricData[self.model_name]['expert_model_2']['coefficients']
        x = np.linspace(bounds[0][0],bounds[0][1],50)
        y = np.linspace(bounds[1][0],bounds[1][1],50)
        X,Y = np.meshgrid(x,y)
        Z = np.array(X)
        for i in range(len(x)):
            for j in range(len(y)):
                print(i,j)
                time_a = time.time()
                point = [X[i,j],Y[i,j]]
                for k in range(len(self.Points)):
                    for l in range(len(self.Points)):
                        p1 = self.Points[k]
                        p2 = self.Points[l]
                        Mean[k] , CoVarianceMatrix[k,l] = self.EvaluateParametricExpertGaussianProcess(p1,p2,'expert_model_2',point)
                CoVarianceInverseMatrix = np.linalg.pinv(CoVarianceMatrix)
                Z[i,j] = self.LogLikelihood(CoVarianceMatrix,CoVarianceInverseMatrix,Mean)
        import matplotlib.pyplot as plt
        from mpl_toolkits.mplot3d import Axes3D
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot_surface(X, Y, Z, cmap=plt.cm.YlGnBu_r)
        plt.show()
        input()
        #######################################
        """
        for exp_model_name in list(self.ExpertGaussianProcesss):
            if self.ExpertGaussianProcesss[exp_model_name]['source'] == 'parametric':
                print("Optimizing parametric model: ", exp_model_name)
                bounds = self.data.ParametricData[self.model_name][exp_model_name]['coefficients']
                ObjectiveFunction = self.LogLikelihoodObjective
                #a,b,c = opt.PerformEvolutionOptimization(bounds,ObjectiveFunction,exp_model_name)
                #Coefficients[exp_model_name] = a
                Coefficients[exp_model_name] = np.array([1.0,5.0])
        return Coefficients

    def LogLikelihood(self,C,C_Inv,Mean):
        y = self.ModelFunctionEvals
        sign,logdet = np.linalg.slogdet(C)
        #return (-0.5 * ((y-Mean).dot(C_Inv).dot(y-Mean))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi)) 
        return (-0.5 * ((y).dot(C_Inv).dot(y))) - (0.5 * sign * logdet) - ((self.dim/2.0)*np.log(2.0*np.pi)) 

    def LogLikelihoodObjective(self,trial_coefficients,exp_model_name):
        CoVarianceMatrix = np.zeros((len(self.Points),len(self.Points)))
        Mean = np.zeros((len(self.Points)))
        for i in range(len(self.Points)):
            for j in range(len(self.Points)):
                p1 = self.Points[i]
                p2 = self.Points[j]
                Mean[i] , CoVarianceMatrix[i,j] = self.EvaluateParametricExpertGaussianProcess(p1,p2,exp_model_name,trial_coefficients)
        CoVarianceInverseMatrix = np.linalg.pinv(CoVarianceMatrix)
        return self.LogLikelihood(CoVarianceMatrix,CoVarianceInverseMatrix,Mean),0


    def EvaluateParametricExpertGaussianProcess(self,p1,p2,exp_model_name,coefficients):
        fv,variance = self.ExpertFunctions[exp_model_name](p1,p2,
                        coefficients,
                        self.data.ParametricData[self.model_name][exp_model_name]['variances'])
        return fv,variance

    def EvaluatePointExpertGP(self,p,exp_model_name):
        PointNumber = self.ExpertGP[exp_model_name].PointNumber
        k  = np.zeros((PointNumber))
        K     = self.ExpertGP[exp_model_name].K
        K_Inv = self.ExpertGP[exp_model_name].K_Inv
        Model = self.ExpertGP[exp_model_name].Model
        Points = self.ExpertGP[exp_model_name].Points
        for i in range(PointNumber):
            k[i] = self.ExpertGP[exp_model_name].KernelMaternND(Points[i],p,self.ExpertGP[exp_model_name].HyperParameters)
        return (k.dot(K_Inv)).dot(Model),\
               self.EvaluatePointExpertVariance(p, exp_model_name)


        return 
    def EvaluatePointExpertVariance(self,p,exp_model_name):
        a = self.ExpertGP[exp_model_name]
        PointNumber = a.PointNumber
        K = a.K
        K_Inv = a.K_Inv

        k = np.zeros((PointNumber))
        for i in range(PointNumber):
            k[i] = a.KernelMaternND(a.Points[i],p,a.HyperParameters)
        return a.KernelMaternND(p,p,a.HyperParameters) - (k.dot(K_Inv)).dot(k)

    def LoadParametricExpertFunctions(self):
        #the parametric model should be given a point and should 
        #return a mean and covariance matrix
        import sys
        f = {}
        for exp_model_name in list(self.ExpertGaussianProcesss):
            if self.ExpertGaussianProcesss[exp_model_name]['source'] == 'parametric':
                path = self.data.ParametricData[self.model_name][exp_model_name]['path']
                module = self.data.ParametricData[self.model_name][exp_model_name]['module']
                function = self.data.ParametricData[self.model_name][exp_model_name]['function']
                sys.path.insert(0,path)
                f[exp_model_name] = self.data.importName(module,function)
        return f

