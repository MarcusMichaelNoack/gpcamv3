###########################
##Surrogate Model Class####
###########################

import random
import numpy as np
import Config as conf
import Misc as smc
from Kernel import *
from Kernel import Kernel as kernel
import ExperimentControls as exp
import itertools
import matplotlib.pyplot as plt
from MeasurementCosts import Cost as costs
import Optimization as opt
import Misc as smc
from scipy.interpolate import griddata

#####A class to combine the data models with all their respective
#####expert models

class SurrogateModel:
    def __init__(self,GPs,Data,costs,switch):
        self.GPs = GPs
        self.Data = Data
        self.PointNumber = Data.PointNumber
        self.Points = Data.Points
        self.switch = switch
        self.bounds = Data.bounds
        self.AveragePointDistance = Data.AveragePointDistance
        self.EvaluateMeanGradient = GPs['experiment data'].EvaluateMeanGradient
        self.experiment_costs = costs


    def update(self,GPs,Data,costs):
        self.GPs = GPs
        self.Data = Data
        self.experiment_costs = costs

    def EvaluateSurrogateModelFunction(self,p):
        if self.switch == 'on':
            mean_domain, variances_domain,expert_model_distribution,x = self.EvaluateDomainExpertModel(p)
            mean_data, variance_data = self.EvaluateDataSurrogateModel(p)
            if variance_data < 0.001: variance_data = 0.001
            l = mean_data-(4.0*np.sqrt(variance_data))
            u = mean_data+(4.0*np.sqrt(variance_data))
            a = min(min(x),l)
            b = max(max(x),u)
            x1 = np.linspace(a,b,10000)
            dx = abs(a-b)/10000.0
            inter = griddata(x,expert_model_distribution,x1,method='linear',fill_value = 0)
            f = smc.NormedGaussianFunction(x1,mean_data,variance_data)
            f = f/(sum(f)*dx)
            inter = inter/(sum(inter)*dx)
            res = (f+inter)/(sum(f+inter)*dx)
            mean = sum(x1*res)*dx
            if max(f) != max(f):print("NaN in SurrogateModel.py");exit()
            return mean,sum(((x1-mean)**2)*res)*dx
        elif self.switch == 'off':
            return self.EvaluateDataSurrogateModel(p)
        else:
            print("Domain expert switch not defined, exit."); exit()

    def EvaluateDomainExpertSurrogateModel(self,p):
        sigma2 = {}
        mean = {}
        bounds = self.GPs['expert knowledge'].bounds
        s = np.zeros((len(self.GPs['expert knowledge'].Gaussians)))
        m = np.zeros((len(self.GPs['expert knowledge'].Gaussians)))
        mean["expert knowledge"]   = {}
        sigma2["expert knowledge"] = {}
        y = np.zeros((len(m),10000))
        x = np.linspace(bounds[0],bounds[1],10000)
        dx = abs(bounds[0]-bounds[1])/10000.0
        index = 0
        for expert_name in list(self.GPs['expert knowledge'].Gaussians):
            mean["expert knowledge"][expert_name],sigma2["expert knowledge"][expert_name] =\
            self.GPs['expert knowledge'].EvaluateExpertGP(expert_name,p)
            m[index] = mean["expert knowledge"][expert_name]
            s[index] = sigma2["expert knowledge"][expert_name]
            y[index] = smc.NormedGaussianFunction(x,m[index],s[index])
            for j in range(len(x)):
                if x[j] < bounds[0] or x[j] > bounds[1]: y[:,j] = 0.0
            y[index] = y[index]/(sum(y[index])*dx)
            index = index + 1

        ###here we compute the probability of each expert model
        ###given the most likely data, this is probably not correct and needs 
        ###more thinking
        #p = np.zeros((len(m)-1))
        #for i in range(len(m)-1):
        #    p[i] = self.EvaluateMeanProbability(y[0],y[i+1],dx)
        #print(p)
        ########################################################
        comb = np.ones((10000))
        for i in range(len(y)):
            comb = comb + y[i]
        comb = comb / (sum(comb)*dx)

        max_index = np.argmax(comb)
        comb_mean = sum(x*comb)*dx
        return comb_mean,sum(((x-comb_mean)**2)*comb)*dx,comb,x

    def EvaluateDataSurrogateModel(self,p):
        return self.GPs['experiment data'].EvaluateMean(p),self.GPs['experiment data'].EvaluateVariance(p)


    #def EvaluateMeanProbability(self,y1,y2,dx):
    #    ##compute the Bhattacharyya distance
    #    return sum(np.sqrt(y1*y2)) * dx


###########################################################################
###########################################################################
###########################################################################
    def FindModelMinimum(self,info):
        if conf.ModelOptimizationMethod == 'HGDN':
            if info == 'all_elements':
                [opti,func_eval,error] = opt.HGDN(self.bounds[0],self.EvaluateMean,self.EvaluateMeanGradient,self.EvaluateMeanHessian)
            else:
                [opti,func_eval,error] = opt.HGDN(self.bounds[1],self.EvaluateMean,self.EvaluateMeanGradient,self.EvaluateMeanHessian)
            optimum = self.Data.CompressIntoData(opti,error)
            return optimum
        elif conf.ErrorOptimizationMethod == 'Genetic':
            if info == 'all_elements':
                [opti,func_eval,error] = opt.PerformEvolutionOptimization(self.bounds[0],self.EvaluateMean)
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            else:
                [opti,func_eval,error] = opt.PerformEvolutionOptimization(self.bounds[1],self.EvaluateMean)
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            return optimum

    def FindModelMaximum(self,info):
        if conf.ModelOptimizationMethod == 'HGDN':
            if info == 'all_elements':
                [opti,func_eval,error] = opt.HGDN(self.bounds[0],self.EvaluateMean,self.EvaluateMeanGradient,self.EvaluateMeanHessian)
            else:
                [opti,func_eval,error] = opt.HGDN(self.bounds[1],self.EvaluateMean,self.EvaluateMeanGradient,self.EvaluateMeanHessian)
            optimum = self.Data.CompressIntoData(opti,error)
            return optimum
        elif conf.ErrorOptimizationMethod == 'Genetic':
            if info == 'all_elements':
                [opti,func_eval,error] = opt.PerformEvolutionOptimization(self.bounds[0],self.EvaluateMean)
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            else:
                [opti,func_eval,error] = opt.PerformEvolutionOptimization(self.bounds[1],self.EvaluateMean)
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            return optimum

    def FindErrorMaximum(self,info):
        Population = opt.InitPopulation(self.bounds[0],conf.population_size,len(self.bounds))
        self.c,a = self.ComputeGradientWeight(Population)
        self.d,f = self.ComputeTargetWeight(Population)

        if conf.ErrorOptimizationMethod == 'HGDN':
            if info == 'all_elements':
                [opti,func_eval] = \
                opt.HGDN(self.bounds[0],self.ComputeObjective,self.ComputeObjectiveGradient,self.ComputeObjectiveHessian)
                error = func_eval[:,1]
                optimum = self.Data.CompressIntoData(opti,error)
            else:
                [opti,func_eval] = \
                opt.HGDN(self.bounds[1],self.ComputeObjective,self.ComputeObjectiveGradient,self.ComputeObjectiveHessian)
                error = func_eval[:,1]
                optimum = self.Data.CompressIntoData(opti,error)
            return optimum

        elif conf.ErrorOptimizationMethod == 'Genetic':
            if info == 'all_elements':
                [opti,func_eval] = opt.PerformEvolutionOptimization(self.Data.bounds[0],self.ComputeObjective,0)
                error = func_eval[1]
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            else:
                opti,func_eval = opt.PerformEvolutionOptimization(self.bounds[1],self.ComputeObjective,0)
                error = func_eval[1]
                optimum = self.Data.CompressIntoData(np.array([opti]),np.array([error]))
            return optimum
###########################################################################
###########################################################################
###########################################################################
    def ComputeGradientWeight(self,set_of_points):
        if conf.GradientMode == False:
            return 0,0
        IndGrad = np.zeros((len(set_of_points)))

        for i in range(len(set_of_points)):
            IndGrad[i] = np.linalg.norm(self.EvaluateMeanGradient(set_of_points[i]))
        if conf.SimpleDistributionSelection[0] == False:
            gdc = [conf.GradientDistributionCoefficients[0],\
                   conf.GradientDistributionCoefficients[1],\
                   conf.GradientDistributionCoefficients[2],\
                   conf.GradientDistributionCoefficients[3],\
                  ]
        elif conf.SimpleDistributionSelection[0] == True:
            gdc = [0.8,1.0,0.01,0.5]
        else:
            print("No gradient distribution preference chosen, exit")
            exit()
        d = smc.MakeDistribution(IndGrad)
        #plt.plot(d)
        #plt.xlabel("scaled gradient")
        #plt.ylabel("probability density")
        integral = sum(\
        d[int(100*gdc[0]):\
        int(100*gdc[1])])\
                    *0.01
        if integral < gdc[2]:
            c = 0
        elif integral > gdc[3]:
            c = 0.0
        else:
            delta = abs(gdc[3]-gdc[2])
            c = (integral-gdc[2])/delta
        if conf.SimpleDistributionSelection[0] == True: c = c*conf.SimpleDistributionSelection[1]
        #print('c: ',c,'integral: ',integral,' gdc: ',gdc)
        #plt.show()
        #input()
        return c,integral
    def ComputeTargetWeight(self,set_of_points):
        if conf.TargetMode == False:
            return 0,0
        IndSurrogate = np.zeros((len(set_of_points)))
        for i in range(len(set_of_points)):
            IndSurrogate[i] = self.GPs['experiment data'].EvaluateMean(set_of_points[i])

        if conf.SimpleDistributionSelection[0] == False:
            tdc = [conf.TargetDistributionCoefficients[0],\
                   conf.TargetDistributionCoefficients[1],\
                   conf.TargetDistributionCoefficients[2],\
                   conf.TargetDistributionCoefficients[3],\
                   ]
        elif conf.SimpleDistributionSelection[0] == True:
            tdc = [0.8,1.0,0.0,0.5]
        else:
            print("No target distribution preference chosen, exit")
            exit()

        d = smc.MakeDistribution(IndSurrogate)
        integral = sum(\
        d[int(100*tdc[0]):\
        int(100*tdc[1])])\
                    *0.01
        if integral < tdc[2]:
            c = 0
        elif integral > tdc[3]:
            c = 0.0
        else:
            delta = abs(tdc[3]-tdc[2])
            c = (integral-tdc[2])/delta
        return c,integral


######################################################
######################################################
######################################################
######################################################
    def ComputeObjective(self,p):
        ###in this function the pure error evaluation gets adjusted for costs and gradients/
        ###or targets
        mean,variance_eval = self.EvaluateSurrogateModelFunction(p)
        if variance_eval < 10e-8: variance_eval = 10e-8
        if conf.Costs == True:
            costs_eval = self.experiment_costs.\
            ComputeLocalCost(p,self.Data.data_set_points[self.PointNumber-1])
        else:
            costs_eval = 1.0
        if conf.GradientMode == True and self.c != 0.0:
            gradient = np.linalg.norm(self.EvaluateMeanGradient(p))
        else:
            gradient = 0.0
        if conf.TargetMode == True and self.d != 0.0:
            target = mean * conf.target_sign
        else:
            target = 0.0
        obj_eval = ((np.sqrt(variance_eval))+(self.c*gradient*self.AveragePointDistance*0.5)+(self.d*target))/costs_eval
        return np.array([obj_eval,np.sqrt(variance_eval)])

    def ComputeObjectiveGradient(self,p):
        ###in this function the pure error gradient gets adjusted for costs and gradients/
        ###or targets
        mean,variance_eval = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
        if variance_eval < 10e-8: variance_eval = 10e-8
        error_g = self.EvaluateVarianceGradient(p)
        if conf.Costs == True:
            costs_eval = self.experiment_costs.\
            ComputeLocalCost(p,self.Data.data_set_points[self.PointNumber-1])
            costs_g = self.experiment_costs_d.\
            ComputeLocalCostGradient(p,self.Data.data_set_points[self.PointNumber-1])
        else:
            costs_eval = 1.0
            costs_g = np.ones((self.dim))
        if conf.GradientMode == True and c != 0.0:
            gradient = self.EvaluateMeanGradient(p)
            gradient_abs = np.linalg.norm(self.EvaluateMeanGradient(p))
            h  = self.EvaluateMeanHessian(p)
            gradient_g = np.zeros(len(h))
            for i in range(len(gradient_g)):
                for j in range(len(gradient_g)):
                    gradient_g[i] = gradient_g[i] + (h[j,i]*gradient[j])
        else:
            gradient_g = np.zeros((self.dim))
            gradient_abs = 1.0
        if conf.TargetMode == True and d != 0.0:
            target_g = self.EvaluateMeanGradient(p) * conf.target_sign
        else:
            target_g = np.zeros((self.dim))

        objective_gradient = ((((1.0-self.c)*(1.0-self.d)*(0.5*error_g)/np.sqrt(variance_eval))+\
                (self.c*(1.0/gradient_abs)*\
                gradient_g*self.AveragePointDistance*0.5)+\
                (self.d*target_g))*costs_g)/(-costs_eval**2)
        return objective_gradient
    def ComputeObjectiveHessian(self,p):
        ###in this function the pure error gradient gets adjusted for costs and gradients/
        ###or targets
        mean,variance_eval = EvaluateVariance(p, self.PointNumber, self.Points, self.C_Inv, self.V, self.C)
        if variance_eval < 10e-8: variance_eval = 10e-8
        error_g = self.EvaluateVarianceGradient(p)
        error_h = self.EvaluateVarianceHessian(p)
        if conf.Costs == True:
            costs_eval = self.experiment_costs.\
            ComputeLocalCost(p,self.Data.data_set_points[self.PointNumber-1])
            costs_g = self.experiment_costs_d.\
            ComputeLocalCostGradient(p,self.Data.data_set_points[self.PointNumber-1])
            costs_h = self.experiment_costs.\
            ComputeLocalCostHessian(p,self.Data.data_set_points[self.PointNumber-1])
        else:
            costs_eval = 1.0
            costs_g = np.ones((self.dim))
            costs_h = np.ones((self.dim,self.dim))
        #if conf.GradientMode == True and c != 0.0:
        #    gradient_g = np.zeros((self.dim,self.dim))
        #else:
        #    gradient = 0.0
        if conf.TargetMode == True and d != 0.0:
            target_h = self.EvaluateMeanHessian(p) * conf.target_sign
        else:
            target_h = np.zeros((self.dim,self.dim))
        objective_hessian = np.zeros((self.dim,self.dim))
        #print('error_h: ',error_h)
        for i in range(self.dim):
            for j in range(self.dim):
                objective_hessian[i,j] = (
                ((((-0.25/np.sqrt(variance_eval)**3)*error_g[j]*error_g[i])+(0.5*error_h[i,j]/np.sqrt(variance_eval))))\
                +(self.d*target_h[i,j]))*\
                (2.0*costs_g[i]*costs_g[j]*(costs_eval**-3)-\
                (costs_h[i,j]/costs_eval**2))
        return objective_hessian

