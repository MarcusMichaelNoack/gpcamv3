import Misc as smc
from scipy.optimize import curve_fit
import matplotlib.pyplot as plt
import numpy as np
import math
import Config as conf
import numba_wrapper as nb


@nb.njit(**nb.default_conf)
def Vario(x,m,a):
    return m*(1.0-(np.exp(-a*x)))
    #return m*(1.0-(np.exp(-a*x**2)))

@nb.njit(**nb.default_conf)
def VarioDerivative(x,m,a):
    e = np.exp(-a*x)
    return m*a*e
    #return 2.0*x*m*a*e

@nb.njit(**nb.default_conf)
def VarioHessian(x,m,a):
    e = np.exp(-a*x)
    return -m*(a**2)*e

#We define this in python because curve_fit doesn't perform well with a numba jitted callback
def FitFunction(x,m,a):
    return m*(1.0-(np.exp(-a*x)))

def CurveFit(Vario, DataArray, MaxData, MaxDist):

    return curve_fit(  FitFunction, DataArray[0], \
                       DataArray[1], p0 = [MaxData,1.0/MaxDist],\
                       bounds=([0,0],[np.inf,np.inf]), ftol = 1e-6, xtol = 1e-6 , maxfev=1000000)

@nb.njit(fastmath=True, cache=True)
def Covariance(Points, V, len_Points):
    #Computes the Kriging Covariance and its inverse for a certain model
    CoVariance=np.zeros((len_Points, len_Points))
    for j in range(len_Points):
        for k in range(j,len_Points):
            Dist=smc.L2Norm(Points[j],Points[k])
            CoVariance[j,k] = V[0] - Vario(Dist, V[0], V[1])


    #save time by mirroring, not computing
    for j in range(1,len_Points):
        for k in range(0,j):
            CoVariance[j,k]=CoVariance[k,j]

    CoVarianceInverse = np.linalg.inv(CoVariance)
    return CoVariance,CoVarianceInverse


def ComputeCovariance(Points, V, len_Points):
    try:
        return Covariance(Points, V, len_Points)
    except:
        print(Points)
        print("C Matrix singular")
        print("That normally means that points in P where measured more than once.")
        print("EXIT")
        exit()


@nb.jit(**nb.default_conf)
def GenVarioData(Model, Points, CoefN, NumberPoints):

    DataArray=np.zeros((2,CoefN))
    counter=0

    for idx in range(NumberPoints):
        for jdx in range (idx+1,NumberPoints):

            h=smc.L2Norm(Points[idx], Points[jdx])
            DeltaZ=((Model[idx]-Model[jdx])**2)/2.0
            DataArray[0,counter] = h
            DataArray[1,counter] = DeltaZ
            counter=counter+1
    MaxData = np.max(DataArray[1])
    MaxDist = np.max(DataArray[0])

    return DataArray, MaxData, MaxDist


def ComputeVariogram(Model, Points, CoefN, NumberPoints):
    DataArray, MaxData, MaxDist = GenVarioData(Model, Points, CoefN, NumberPoints)

    try:
        popt, pcov = CurveFit(Vario, DataArray, MaxData, MaxDist)
    except Exception as e:
        print("curve fit not successful")
        print('Error Message: ', e)
        popt = [MaxData,1.0/MaxDist]
    """
    print(MaxDist)
    print("Variogram Coefficient: ",popt)
    plt.scatter(DataArray[0],DataArray[1])
    xx=np.linspace(0,MaxDist,100000);
    yy=Vario(xx,*popt)
    plt.plot(xx,yy)
    plt.xlabel("Euclidean Distance [arbitrary unit]")
    plt.ylabel("Normed Correlation [model unit]")
    plt.show()
    """

    return popt


class Variogram:
    def __init__(self,points,model):
        self.Points = points    ####points as array
        self.Model  = model     ####function values as array
        self.NumberPoints = len(self.Points)
        self.VariogramCoefficients = self.ComputeVariogramCoefficients()
        self.C,self.C_Inv = ComputeCovariance(self.Points,self.VariogramCoefficients,len(self.Points))
    def UpdateVariogram(self,points,model,mode):
        self.Points = points
        self.Model  = model
        self.NumberPoints = len(self.Points)
        if mode == 'full': self.VariogramCoefficients = self.ComputeVariogramCoefficients()
        self.C,self.C_Inv = ComputeCovariance(self.Points,self.VariogramCoefficients,len(self.Points))
    def ComputeVariogramCoefficients(self):
        VariogramCoefficients=np.zeros((2))
        HNumber = int(((self.NumberPoints**2)-self.NumberPoints)/2)
        VariogramCoefficients= ComputeVariogram(self.Model,self.Points,HNumber, self.NumberPoints)
        return VariogramCoefficients

