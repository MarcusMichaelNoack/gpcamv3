import time
from time import sleep
import random
import numpy as np
import Config as conf
import ExperimentControls as exp
from random import seed
from random import randint
import itertools
import Misc as smc
import numba_wrapper as nb

#@nb.njit(**nb.default_conf)
def AveragePointDistance(Points):
    distance = 0.0
    neighbor_dist = np.zeros((len(Points)))

    for idx1 in range(len(Points)):
        min_neighbor_distance = np.inf
        for idx2 in range(len(Points)):
            dist = smc.L2Norm(Points[idx1], Points[idx2])

            if idx1 == idx2: continue
            if min_neighbor_distance > dist:
                neighbor_dist[idx1] = dist
                min_neighbor_distance = dist
    return np.mean(neighbor_dist)

class ExperimentData:
    def __init__(self, initialize = True, InitDataSet = {}):
        if conf.TimeSeries[0] == True: self.DefineParameters()
        self.PointNumber = conf.initial_data_set_size
        if initialize == True: self.data_set = self.InitializeData()
        elif initialize == False: self.data_set = InitDataSet
        self.PointNumber = len(self.data_set)
        self.dim = len(conf.Parameters)
        self.bounds = self.ExtractBounds()
        self.data_set_points,self.data_set_models,\
        self.data_set_variances, self.data_set_costs = self.SeperateModelAndPoints()
        self.ExistenceList = self.MakeExistenceList()
        self.DemandList = self.MakeDemandList()
        self.StartTime = time.time()
        if len(self.data_set_points) > 1:
            self.AveragePointDistance = self.ComputeAveragePointDistance()
        self.Points = self.ExtractPointsFromData()
        self.Models = self.ExtractModelFromData()
        self.Variances = self.ExtractVarianceFromData()
        self.MeasurementCosts = self.ExtractCostsFromData()

    def UpdateData(self,NewData):
        self.LatestData = self.UpdateDataSet(NewData)
        self.PointNumber = len(self.data_set)
        self.UpdateExistenceList()
        self.data_set_points,self.data_set_models,\
        self.data_set_variances, self.data_set_costs = self.SeperateModelAndPoints()
        if len(self.data_set_points) > 1:
            self.AveragePointDistance = self.ComputeAveragePointDistance()
        self.Points = self.ExtractPointsFromData()
        self.Models = self.ExtractModelFromData()
        self.Variances = self.ExtractVarianceFromData()
        self.MeasurementCosts = self.ExtractCostsFromData()

################################################################
################################################################
################################################################
    def DefineParameters(self):
        NewParameters = {}
        for time_stamp in conf.TimeSeries[1]:
            for parameter in conf.Parameters:
                NewParameters[parameter+"_"+str(time_stamp)] = dict((conf.Parameters[parameter]))
                NewParameters[parameter+"_"+str(time_stamp)]['time stamp'] = time_stamp
        conf.Parameters = NewParameters
    def importName(self,modulename, name):
        """ Import a named object from a module in the context of this function.
        """
        try:
            module = __import__(modulename, globals(), locals(  ), [name])
        except ImportError:
            return None
        return vars(module)[name]


    def ExtractPointsFromData(self):
        P = np.zeros((self.PointNumber,self.dim))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.Parameters):
                P[idx,index] = self.data_set[idx][name]
                index += 1
        return P
    def ExtractModelFromData(self):
        M = np.zeros((self.PointNumber,len(conf.ModelFunctions)))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.ModelFunctions):
                M[idx,index] = self.data_set[idx][name]
                index +=1 
        return M

    def ExtractVarianceFromData(self):
        Variance = np.zeros((self.PointNumber,len(conf.ModelFunctions)))
        for idx in range(self.PointNumber):
            index = 0
            for name in list(conf.ModelFunctions):
                Variance[idx,index] = self.data_set[idx][name+' variance']
                index +=1
        return Variance

    def ExtractCostsFromData(self):
        Costs = np.zeros((self.PointNumber))
        for idx in range(self.PointNumber):
            Costs[idx] = self.data_set[idx]['cost']
        return Costs

    def CompressIntoData(self,Points,FunctionValues):
        data = {}
        for i in range(len(Points)):
            data[i] = {}
            index = 0
            for name in list(conf.Parameters):
                data[i][name] = Points[i,index]
                index = index + 1
            data[i]['error_value'] = FunctionValues[i]
        return data

    def ExtractBounds(self):
        Bounds = [[],[]]
        for name in list(conf.Parameters):
            if conf.Parameters[name]['property'] == 'connected':
                Bounds[0].append(conf.Parameters[name]['elements'])
                Bounds[1].append(conf.Parameters[name]['prepared_elements'])
            elif  conf.Parameters[name]['property'] == 'disconnected':
                Bounds[0].append([np.min(conf.Parameters[name]['elements']),\
                        np.max(conf.Parameters[name]['elements'])])
                Bounds[1].append([np.min(conf.Parameters[name]['prepared_elements']),\
                        np.max(conf.Parameters[name]['prepared_elements'])])
            elif  conf.Parameters[name]['property'] == 'discrete':
                Bounds[0].append([np.min(conf.Parameters[name]['elements']),\
                        np.max(conf.Parameters[name]['elements'])])
                Bounds[1].append([np.min(conf.Parameters[name]['prepared_elements']),\
                        np.max(conf.Parameters[name]['prepared_elements'])])
        return Bounds

    def UpdateDataSet(self,NewData):
        self.data_set.update(NewData)
        self.data_set = exp.BlackBox(self.data_set)
        UpdatedNewData = {}
        for idx in NewData:
            UpdatedNewData[idx] = {}
            for entry in NewData[idx]:
                UpdatedNewData[idx][entry] = self.data_set[idx][entry]
        return UpdatedNewData

    def SeperateModelAndPoints(self):
        data_set_points = {}
        data_set_models = {}
        data_set_variances = {}
        data_set_costs = {}
        #print(self.data_set)
        for idx in self.data_set:
            data_set_points[idx] = {}
            data_set_models[idx] = {}
            data_set_variances[idx] = {}
            try: data_set_costs[idx] = self.data_set[idx]['cost']
            except: pass
            for parameter in list(conf.Parameters):
                data_set_points[idx][parameter] = self.data_set[idx][parameter]
            for model_name in list(conf.ModelFunctions):
                data_set_models[idx][model_name] = self.data_set[idx][model_name]
                try: data_set_variances[idx][model_name] = \
                        self.data_set[idx][model_name+' variance']
                except: pass

        return data_set_points,data_set_models, data_set_variances,data_set_costs

    def InitializeData(self):
        if conf.ReadData == True:
            data = self.ReadDataFromFile()
            for idx in data:
                data[idx]['measured']  = True
        elif conf.ReadData == False:
            data = self.CreateRandomData()
        else:
            print("No data input method specified, exit.")
            exit()
        return data

    def ReadDataFromFile(self):
        if conf.ask_for_file == False:
            print("Read initial data from ",conf.data_file)
            data = np.load(conf.data_file).item()
        else:
            while FilesExist == False and counter < 100:
                counter = counter + 1
                try:
                    File1 = input("What is the path to the data file?")
                    data = np.load(File1).item()
                    FilesExist = True
                except:
                    FilesExist = False
                    print("Paths not correctly given. Try again!")
        return data

    def CreateRandomData(self):
        data = {}
        point_index = 0
        while len(data) < self.PointNumber:
            data[point_index] = {}
            for parameter in conf.Parameters:
                if conf.Parameters[parameter]['property'] == \
                'connected':
                    lower_limit = \
                    conf.Parameters[parameter]['prepared_elements'][0]
                    upper_limit = \
                    conf.Parameters[parameter]['prepared_elements'][1]
                    data[point_index][parameter] = random.uniform(lower_limit,upper_limit)
                elif conf.Parameters[parameter]['property'] == \
                'discrete':
                    length = \
                    len(conf.Parameters[parameter]['prepared_elements'])
                    data[point_index][parameter] = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [randint(0,length-1)]
                elif conf.Parameters[parameter]['property'] == \
                'disconnected':
                    length = \
                    len(conf.Parameters[parameter]['prepared_elements'])
                    random_number = randint(0,length-1)
                    lower_limit = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [random_number][0]
                    upper_limit = \
                    conf.Parameters[parameter]['prepared_elements']\
                    [random_number][1]
                    data[point_index][parameter] = random.uniform(lower_limit,upper_limit)
                else:
                    print("no valid property given. exit")
                    exit()
            data[point_index]["measured"] = False
            if conf.Costs == True:
                data[point_index]['cost'] = 0.0
            else:
                data[point_index]['cost'] = None
            for model in conf.ModelFunctions:
                data[point_index][model+' variance'] = 0.0
                data[point_index][model] = None

            point_index = len(data)
        data = exp.BlackBox(data)
        self.PrintData(data)
        return data
###############################################################
###############################################################
###############################################################
    def SnapToClosestPoint(self,Value,para_name,s):
        Array = conf.Parameters[para_name][s]
        distance_1 = np.inf
        for i in range(0,len(Array)):
            distance = abs(Array[i] - Value)
            if distance < distance_1:
                NewValue = Array[i]
            else:
                break
            distance_1 = distance

        return NewValue

    def SnapToClosestInterval(self,Value,para_name,s):
        Array = conf.Parameters[para_name][s]
        distance_1 = np.inf
        for i in range(0,len(Array)):
            if len(Array[i]) == 1:
                distance = abs(Array[i] - Value)
                if distance < distance_1:
                    NewValue = Array[i]
                else:
                    break
                distance_1 = distance
            elif len(Array[i]) == 2:
                if Value >= Array[i][0] and Value <= Array[i][1]:
                    return Value
                distance1 = abs(Array[i][0] - Value)
                distance2 = abs(Array[i][1] - Value)
                if distance1 < distance2:
                    if distance1 < distance_1:
                        NewValue = Array[i][0]
                    else:
                        break
                    distance_1 = distance1
                elif distance2 <= distance1:
                    if distance2 < distance_1:
                        NewValue = Array[i][1]
                    else:
                        break
                    distance_1 = distance2
            else:
                print('Parameter ranges not correctly given. 3 values given')
        return NewValue
    def Snap(self,Optimum,s):
        ###Snapping
        for idx in list(Optimum):
            for para_name in conf.Parameters:
                if conf.Parameters[para_name]['property'] == 'discrete':
                    Optimum[idx][para_name] = \
                    self.SnapToClosestPoint(Optimum[idx][para_name],para_name,s)
                elif conf.Parameters[para_name]['property'] == 'disconnected':
                    Optimum[idx][para_name] = \
                    self.SnapToClosestInterval(Optimum[idx][para_name],para_name,s)
                elif conf.Parameters[para_name]['property'] == 'connected':
                    continue
        return Optimum
###############################################################
###############################################################
###############################################################
    def MakeUnique(self,data):
        unique = {}
        data_a = smc.Dict2D_2_nparray(data)
        data_a = np.unique(data_a, axis = 0)
        param = list(data[0].keys())
        data = { a:{ param[b] : data_a[a, b] for b in range(len(param))} for a in range(data_a.shape[0])}
        return data

    def SaveDemandList(self,DemandList):
        import json
        with open('DemandList.txt', 'w') as file:
              file.write(json.dumps(DemandList))
        return 0

    def LoadDemandList(self):
        import json
        with open('DemandList.txt', 'r') as file:
              file.read(json.loads(DemanList))
        return DemandList

    def MakeDemandList(self):
        DemandList = {}
        DemandList[0] = {}
        return DemandList

    def MakeExistenceList(self):
        ExistenceDict = {}
        space = [[]]
        index = 0
        for parameter in list(conf.Parameters):
            if conf.Parameters[parameter]['property'] == 'discrete':
                space[index].append(conf.Parameters[parameter]['prepared_elements'])
        ExistenceList = list(itertools.product(*space[0]))
        print('check out the existence list:')
        for idx in range(len(ExistenceList)):
            ExistenceDict[idx] = {}
            index = 0
            for parameter in list(conf.Parameters):
                if conf.Parameters[parameter]['property'] == 'discrete':
                    ExistenceDict[idx][parameter] = ExistenceList[idx][index]
                    index =+ 1
        self.PrintData(ExistenceDict)
        return ExistenceDict

    def UpdateExistenceList(self):
        if conf.WaitingTimeOut[0] == False or not self.DemandList or not self.DemandList[0]:
            return 0
        print('Waiting... please press Ctrl-C if you want to wish to update the samples.')
        try:
            for i in range(0, conf.WaitingTimeOut[1]):
                sleep(1)
            print("")
        except KeyboardInterrupt:
            print("Look at the Demand List and choose samples to prepare. Prepare selected samples.")
            self.PrintData(self.DemandList)
            add = 'y'
            counter = 0
            LengEx = len(self.ExistenceList)
            while add == 'y':
                for name in conf.Parameters:
                    if conf.Parameters[name]['property'] == 'discrete':
                        dec = input("would like to make changes to the elements of "+name+"? (y/n)")
                        if dec =='y':
                            self.ExistenceList[LengEx+counter] = {}
                            self.ExistenceList[LengEx+counter][name] = float(input(name+ " of newly prepared sample:"))
                    if conf.Parameters[name]['property'] == 'connected' and \
                       conf.Parameters[name]['elements'] != conf.Parameters[name]['prepared_elements']:
                        dec = input("would like to make changes to the elements of "+name+"? (y/n)")
                        if dec =='y':
                            print('Old prepared_elements of ',name,': ',conf.Parameters[name]['prepared_elements'])
                            print('Define new prepared_parameters separated by space')
                            conf.Parameters[name]['prepared_elements'] = [float(x) for x in input().split()]
                    if conf.Parameters[name]['property'] == 'disconnected' and \
                       conf.Parameters[name]['elements'] != conf.Parameters[name]['prepared_elements']:
                        dec = input("would like to make changes to the elements of "+name+"? (y/n)")
                        if dec == 'y':
                            print('Old prepared_elements of ',name,': ',conf.Parameters[name]['prepared_elements'])
                            conf.Parameters[name]['prepared_elements'] = []
                            n = input("How many elements would you like to define?")
                            for i in range(int(n)):
                                conf.Parameters[name]['prepared_elements']
                                print('Define ' , i,'the new prepared_parameters separated by space')
                                conf.Parameters[name]['prepared_elements'].append([float(x) for x in input().split()])
                            print("New prepared_elements of ",name,": ",conf.Parameters[name]['prepared_elements'])
                counter += 1
                add =  input("would you like to add more samples (y/n)")

        print('Existence List Updated')
        print(self.ExistenceList)
        for idx1 in list(self.ExistenceList):
            for idx2 in list(self.DemandList):
                if not self.DemandList[idx2]: continue
                if [self.ExistenceList[idx1][x] for x in conf.Parameters if conf.Parameters[x]['property'] == 'discrete'] == \
                   [self.DemandList[idx2][x] for x in conf.Parameters if conf.Parameters[x]['property'] == 'discrete']:
                       del self.DemandList[idx2]
                       break
        index = 0
        for idx in list(self.DemandList):
            self.DemandList[index] = self.DemandList.pop(idx)
            index += 1

    def UpdateDemandList(self,DemandList,NewPoint):
        NewPoint['importance'] = 1.0
        for idx in DemandList:
            if DemandList[idx]: continue
            else: DemandList[idx] = NewPoint; return DemandList
        DemandList[len(DemandList)] = NewPoint
        for idx1 in list(DemandList):
            for idx2 in list(DemandList):
                if idx1 == idx2: continue
                if [DemandList[idx1][x] for x in conf.Parameters if conf.Parameters[x]['property'] == 'discrete'] == \
                   [DemandList[idx2][x] for x in conf.Parameters if conf.Parameters[x]['property'] == 'discrete']:
                       DemandList[idx2]['importance'] = DemandList[idx2]['importance'] + DemandList[idx1]['importance']
                       del DemandList[idx1]
                       break
        index = 0
        for idx in list(DemandList):
            DemandList[index] = DemandList.pop(idx)
            index += 1
        return DemandList

    def PrintData(self,data):
        np.set_printoptions(precision=5)
        for key,val in list(data.items()):
            print(key, "=>", val)

    def DictToPoint(self,Dict):
        point = np.zeros((len(conf.Parameters)))
        index = 0
        for name in list(conf.Parameters):
            point[index] = Dict[name]
            index = index + 1
        return point

    def ComputeAveragePointDistance(self):
        Points = smc.Dict2D_2_nparray(self.data_set_points)
        return AveragePointDistance(Points)

###############################################################
###############################################################
###############################################################
###############################################################
###############################################################
###############################################################

