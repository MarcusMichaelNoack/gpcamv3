import sys
import os
import time
from time import gmtime, strftime
import random
import numpy as np
import Config as conf
import ExperimentControls as ec
import Misc as smc
from DataOK import DataOK as ok
from DataGP import DataGP as gp
from Kernel import Kernel as kernel
from Variogram import Variogram as vario
from DomainExpertGP import DomainExpertGP as DEGP
from SurrogateModel import SurrogateModel as SurrogateModel
from MeasurementCosts import Cost
from ExperimentData import ExperimentData
from DomainExpertData import DomainExpertData as DED
#########################################
######Prepare first set of Experiments###
#########################################
path_new_experiment_command = "./data/new_experiment_command/"
path_new_experiment_result  = "./data/new_experiment_result/"
if os.path.isfile(path_new_experiment_result+"experiment_result.npy"):
    os.remove(path_new_experiment_result+"experiment_result.npy")
if os.path.isfile(path_new_experiment_command+"experiment_command.npy"):
    os.remove(path_new_experiment_command+"experiment_command.npy")
print("################################################")
print("#Surrogate Model Autonomous expeRimenT (SMART)##")
print("################################################")
print("#########Version: 3.0exp(i(pi/2))###############")
print("################################################")
print("")
StartTime = time.time()
StartDateTime = strftime("%Y-%m-%d_%H_%M_%S", time.localtime())
print('Date and time:       ',StartDateTime)
print("################################################")
#########Initialize a set of random measurements####
#########this will be the initial experiment data###
print("Initial data created:")
experiment_data = ExperimentData()
np.save("./data/current_data/Data",experiment_data.data_set)
Time_Length_ParameterSet = np.array([time.time()-StartTime,len(experiment_data.data_set)])
##############################################
###############################################
###Begin GP initialization loop################
###############################################
experiment_costs = Cost(experiment_data)
Kernels = {}
Variograms = {}
GPs = {}
SurrogateModels = {}
expert_data = {}
print("################################################")
print("Initializing data set...")
print("################################################")
index = 0
for model_name in list(conf.ModelFunctions):
    #if conf.ModelFunctions[model_name]['use for steering'] == True:
        print("Initializing the data model for ", model_name," and, if specified, all it's expert models.")
        GPs[model_name] = {}
        if conf.ModelFunctions[model_name]['expert knowledge']['switch'] == 'on':
            ####initialize domain expert knowledge
            print("Initialize expert model for model: ", model_name)
            expert_data[model_name] = DED(model_name,experiment_data)
            GPs[model_name]['expert knowledge'] = \
            DEGP(expert_data[model_name],model_name,experiment_data)
        if conf.Engine == "GP":
            ####initialize the Gaussian Process
            print("Initialize the Gaussian Process for model: ", model_name)
            Kernels[model_name] = kernel(experiment_data.Points,experiment_data.Models[:,index],\
            experiment_data.Variances[:,index],model_name,experiment_data.bounds)
            GPs[model_name]['experiment data'] = \
            gp(experiment_data,experiment_data.Models[:,index],\
            Kernels[model_name],model_name)
        elif conf.Engine == "OK":
            ####initialize Kriging
            Variograms[model_name] = vario(experiment_data.Points,experiment_data.Models[:,index])
            GPs[model_name]['experiment data'] = \
            ok(experiment_data,experiment_data.Models[:,index],\
            Variograms[model_name],model_name,experiment_costs)
        else:
            print("No Engine specified in configuration file, exit."); exit()

        SurrogateModels[model_name] = \
        SurrogateModel(GPs[model_name],experiment_data,experiment_costs,conf.ModelFunctions[model_name]['expert knowledge']['switch'])
        index = index + 1

print("################################################")
print("Initialization concluded, start of SMART data collection")
print("################################################")
###################################################################
#######MAIN LOOP STARTS HERE########################################
####################################################################
error = np.inf
UpdateKernelNumber = 0.0
number_of_measurements = len(experiment_data.data_set)
while error > conf.breaking_error:
    number_of_measurements = len(experiment_data.data_set)
    print("============================================")
    print("============================================")
    print("Total Run Time: ", time.time() -  StartTime, " seconds")
    NextExperimentPoints,Optima = ec.FindNextExperiment(experiment_data,SurrogateModels)
    Prediction = smc.EvaluateAllDataGPs(GPs,NextExperimentPoints,experiment_data)
    Prediction = smc.SimulatePrediction(Prediction,experiment_data,experiment_costs,GPs)
    print("Next measurement points and predictions: ")
    experiment_data.PrintData(Prediction)
    experiment_data.UpdateData(Prediction)
    print("--------------------------------")
    experiment_data.PrintData(experiment_data.data_set)
    print("--------------------------------")
    #input()
    print('')
    print('Found Errors:')
    print([Optima[x][i]['error_value'] for x in Optima for i in Optima[x]])
    print('')
    print('')
    print('')
    if UpdateKernelNumber/number_of_measurements < 1.0-conf.RecomputeKernel:
        UpdateKernelNumber = number_of_measurements
        if conf.Engine == "OK":smc.UpdateAllVariograms(experiment_data,Variograms, 'full'); print("Full Variogram Update")
        elif conf.Engine == "GP":smc.UpdateAllKernels(experiment_data,Kernels,'full'); print("Full Kernel Update")
    else:
        if conf.Engine == "OK":smc.UpdateAllVariograms(experiment_data,Variograms, 'light'); print("Light Variogram Update")
        elif conf.Engine == "GP":smc.UpdateAllKernels(experiment_data,Kernels,'light'); print("Light Kernel Update")

    experiment_costs.Update(experiment_data)
    experiment_costs.AdjustCosts()
    if conf.Engine == "OK": smc.UpdateAllDataGPs(experiment_data,Variograms,GPs,experiment_costs)
    if conf.Engine == "GP": smc.UpdateAllDataGPs(experiment_data,Kernels,   GPs,experiment_costs)
    smc.UpdateAllExpertGPs(GPs,experiment_data,expert_data)
    smc.UpdateAllSurrogateModels(SurrogateModels,GPs,experiment_data,experiment_costs)
    error = max([Optima[x][i]['error_value'] for x in Optima for i in Optima[x]])
    #print("=============================================")
    ######Write Result in File##############
    np.save("./data/historic_data/Data_"+StartDateTime,experiment_data.data_set)
    np.save("./data/current_data/Data",experiment_data.data_set)
    print("Demand List: ")
    experiment_data.PrintData(experiment_data.DemandList)
    experiment_data.SaveDemandList(experiment_data.DemandList)
    print("")
    print("")
    if conf.Limited_Number_of_Measurements == True and \
            number_of_measurements >= conf.Max_Number_of_Measurements:
        print("The maximum number of measurements has been reached")
        break

print('====================================================')
print('The autonomous experiment was concluded successfully')
print('====================================================')
