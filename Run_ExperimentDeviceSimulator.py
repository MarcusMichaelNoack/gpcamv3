import numpy as np
import os
import time

read_file = "./data/new_experiment_command/experiment_command.npy"
write_file = "./data/new_experiment_result/experiment_result.npy"


while True:
    time.sleep(5)
    if os.path.isfile(read_file):
        a = np.load(read_file,encoding="ASCII",allow_pickle = True).item()
        for idx in a:
            a[idx]['measured'] = True
            for name in a[idx]:
                if a[idx][name] == None:
                    a[idx][name] = np.random.rand()

        os.remove(read_file)
        np.save(write_file,a)
